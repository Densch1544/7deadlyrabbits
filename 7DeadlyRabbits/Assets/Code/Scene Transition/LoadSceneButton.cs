using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour
{
    public string sceneName; // Scenennamen wird im Inspector eingetragen
    public void loadSceneButton()
    {
        SceneManager.LoadScene(sceneName); // Eingetragener Name wird geladen
    }
}
