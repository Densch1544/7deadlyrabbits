using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
	//Steht man im Trigger und drückt "E" läd man die im Inspector eingetragene Scene.
	public InputMaster controls;
	public string sceneName;
	public bool canLoad = false;

	void Awake()
	{
		controls = new InputMaster();
		controls.Player.Interact.performed += wectx => Loader(); 
	}

	public void Loader()
    {
		if(canLoad)
        {
			SceneManager.LoadScene(sceneName);
        }
    }
        
	void OnTriggerEnter2D(Collider2D other)
	{
		canLoad = true;
	}

	void OnTriggerExit2D(Collider2D other)
	{
		canLoad = false;
	}

	private void OnEnable()
	{
		controls.Enable();
	}

	private void OnDisable()
	{
		controls.Disable();
	}
}
