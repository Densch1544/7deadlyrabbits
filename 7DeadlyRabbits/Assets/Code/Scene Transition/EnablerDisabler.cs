using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
    public class EnablerDisabler : MonoBehaviour
    {
        public GameObject disabledGO;
        public string sceneName;


        // Update is called once per frame
        void Update()
        {
            if(disabledGO.activeInHierarchy == false)
            {
                SceneManager.LoadScene(sceneName); // Eingetragener Name wird geladen
            }
        }
    }
}
