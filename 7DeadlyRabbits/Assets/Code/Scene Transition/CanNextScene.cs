using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class CanNextScene : MonoBehaviour
    {
        public float timer = 2f;
        private float countDown;

        public BoxCollider2D boxCollider;        

        void Start()
        {
            countDown = timer;
            boxCollider.enabled = false;
        }

        // Nach Timer wird BoxColider eingeschaltet
        void Update()
        {
            countDown -= 1 * Time.deltaTime;

            if(countDown <= 0)
            {
                boxCollider.enabled = true;
            }
        }
    }
}
