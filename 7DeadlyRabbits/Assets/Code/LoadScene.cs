using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
    public class LoadScene : MonoBehaviour
    {
        public string sceneName;
        //L�d szene
        public void LoadThisScene()
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
