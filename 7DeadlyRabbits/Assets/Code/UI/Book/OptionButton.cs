using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace sevensindemon
{
    public class OptionButton : MonoBehaviour
    {
        public GameObject howtoPanel;
        public GameObject backmenuPanel;
        public GameObject exitPanel;
        public GameObject miniGame;
        public GameObject overWorld;
        public GameObject musicPanel;

        public string menuScene;

        public GameObject controllerInput;
        public GameObject keyboardInput;

        //HowtoPanel wird gezeigt
        public void HowTo()
        {
            howtoPanel.SetActive(true);
            backmenuPanel.SetActive(false);
            exitPanel.SetActive(false);
            musicPanel.SetActive(false);
        }

        //HowtoPanel wird nicht gezeigt
        public void NoHowTo()
        {
            howtoPanel.SetActive(false);
        }

        //Steuerung MiniGame wird  gezeigt
        public void RealMiniHowTo()
        {
            miniGame.SetActive(true);
        }

        //Steuerung MiniGame wird nicht gezeigt
        public void NoRealMiniHowTo()
        {
            miniGame.SetActive(false);
        }

        //Steuerung Overworld wird gezeigt
        public void RealOverHowTo()
        {
            overWorld.SetActive(true);
        }

        //Steuerung Overworld wird nicht gezeigt
        public void NoRealOverHowTo()
        {
            overWorld.SetActive(false);
            controllerInput.SetActive(false);
            keyboardInput.SetActive(true);
        }
        public void ShowControllerInput()
        {
            controllerInput.SetActive(true);
            keyboardInput.SetActive(false);
        }

        //BackmenuPanel wird angezeigt
        public void BackMenu()
        {
            backmenuPanel.SetActive(true);
            exitPanel.SetActive(false);
            howtoPanel.SetActive(false);
            musicPanel.SetActive(false);

        }

        //BackmenuPanel wird nicht angezeigt
        public void NoBackMenu()
        {
            backmenuPanel.SetActive(false);

        }

        //Zur�ck zum Titlescreen
        public void RealBackMenu()
        {
            SceneManager.LoadScene(menuScene);

        }

        public void MusicMenu()
        {
            musicPanel.SetActive(true);
            exitPanel.SetActive(false);
            backmenuPanel.SetActive(false);
            howtoPanel.SetActive(false);

        }

        //ExitPanel wird nicht angezeigt
        public void NoMusicMenu()
        {
            musicPanel.SetActive(false);

        }

        //ExitPanel wird angezeigt
        public void ExitGame()
        {
            exitPanel.SetActive(true);
            backmenuPanel.SetActive(false);
            howtoPanel.SetActive(false);
            musicPanel.SetActive(false);

        }

        //ExitPanel wird nicht angezeigt
        public void NoExitGame()
        {
            exitPanel.SetActive(false);

        }

        //Spiel wird beendet
        public void RealExitGame()
        {
            PlayerPrefs.DeleteAll();
            Application.Quit();
        }
    }
}
