using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public string sceneNameStart;
    public string sceneNameMini01;
    public string sceneNameMini02;
    public string sceneNameMini03;
    public string sceneNameMini04;
    public string sceneCredits;

    //Startet das Spiel
    public void PlayGame()
    {
        SceneManager.LoadScene(sceneNameStart);
    }

    //Startet das Minspiel01
    public void PlayMiniGame01()
    {
        SceneManager.LoadScene(sceneNameMini01);
    }

    //Startet das Minspiel02
    public void PlayMiniGame02()
    {
        SceneManager.LoadScene(sceneNameMini02);
    }

    //Startet das Minspiel03
    public void PlayMiniGame03()
    {
        SceneManager.LoadScene(sceneNameMini03);
    }

    //Startet das Minspiel04
    public void PlayMiniGame04()
    {
        SceneManager.LoadScene(sceneNameMini04);
    }

    //Startet das Spiel
    public void ShowCredits()
    {
        SceneManager.LoadScene(sceneCredits);
    }

    //Beendet das Spiel
    public void QuitGame()
    {
        Debug.Log("Spiel wurde beendet!!");
        PlayerPrefs.DeleteAll();
        Application.Quit();
    }
}
