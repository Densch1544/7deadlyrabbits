using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class HowToPlay : MonoBehaviour
    {
        public InputMaster controls;

        public GameObject pageOne;
        public GameObject pageTwo;
        public GameObject pageThree;
        public GameObject pageFour;
        public GameObject pageFive;

        [SerializeField]
        private int index = 0;

        void Awake()
        {
            //New Input System
            controls = new InputMaster();
            controls.Player.TurnOverRight.performed += noctx => TurnOverRight();
            controls.Player.TurnOverLeft.performed += noctx => TurnOverLeft();
        }

        //Index plus eins 
        public void TurnOverRight()
        {
            index += 1;
            Debug.Log("+1");
        }

        //Index minus eins
        public void TurnOverLeft()
        {
            if (index > 0)
            {
                index -= 1;
                Debug.Log("-1");
            }
        }

        //Jede Indexzahl hat einen eigene Case d.h. Buchseite 
        private void Update()
        {
            if (index > 4)
            {
                this.gameObject.SetActive(false);
                index = 0;
            }


            switch (index)
            {
                case 4:
                    //Option
                    pageOne.SetActive(false);
                    pageTwo.SetActive(false);
                    pageThree.SetActive(false);
                    pageFour.SetActive(false);
                    pageFive.SetActive(true);
                    break;

                case 3:
                    //Quest
                    pageOne.SetActive(false);
                    pageTwo.SetActive(false);
                    pageThree.SetActive(false);
                    pageFour.SetActive(true);
                    pageFive.SetActive(false);
                    break;
                case 2:
                    //Inventar
                    pageOne.SetActive(false);
                    pageTwo.SetActive(false);
                    pageThree.SetActive(true);
                    pageFour.SetActive(false);
                    pageFive.SetActive(false);
                    break;
                case 1:
                    //Noitzen
                    pageOne.SetActive(false);
                    pageTwo.SetActive(true);
                    pageThree.SetActive(false);
                    pageFour.SetActive(false);
                    pageFive.SetActive(false);
                    break;
                case 0:
                    //Geschlossen
                    pageOne.SetActive(true);
                    pageTwo.SetActive(false);
                    pageThree.SetActive(false);
                    pageFour.SetActive(false);
                    pageFive.SetActive(false);
                    break;
            }
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
