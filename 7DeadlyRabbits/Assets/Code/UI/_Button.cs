using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace sevensindemon
{

    //One button script, to rule them all
    public class _Button : MonoBehaviour
    {
            public GameObject panel;

        public void DisablePanel()
        {
            panel.SetActive(false);
        }

        public void EnablePanel()
        {
            panel.SetActive(true);
        }

    }



}
