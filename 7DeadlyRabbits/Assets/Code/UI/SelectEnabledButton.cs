using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace sevensindemon
{
    public class SelectEnabledButton : MonoBehaviour
    {
        public Button enabledselectButton;
        public Button disabledselectButton;
        // Start is called before the first frame update
        private void OnEnable()
        {
            EventSystem.current.SetSelectedGameObject(enabledselectButton.gameObject);
            enabledselectButton.OnSelect(new BaseEventData(EventSystem.current));
        }
        private void OnDisable()
        {
            EventSystem.current.SetSelectedGameObject(disabledselectButton.gameObject);
            disabledselectButton.OnSelect(new BaseEventData(EventSystem.current));
        }

    }
}
