using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace sevensindemon
{
    public class ButtonSFX : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, ISelectHandler, ISubmitHandler
    {
        public AudioSource audioSource;
        public AudioClip selectedSound;
        public AudioClip[] submitSound;

        public void OnPointerDown(PointerEventData eventData)
        {
            audioSource.PlayOneShot(selectedSound);
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            audioSource.PlayOneShot(selectedSound);
        }
        public void OnSelect(BaseEventData eventData)
        {
            audioSource.PlayOneShot(selectedSound);
        }
        public void OnSubmit(BaseEventData eventData)
        {
            audioSource.clip = submitSound[Random.Range(0, submitSound.Length)];
            audioSource.Play();
        }
    }
}
