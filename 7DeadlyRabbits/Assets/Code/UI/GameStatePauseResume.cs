using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace sevensindemon
{
    public class GameStatePauseResume : MonoBehaviour
    {
        public InputMaster controls;

        public GameObject pauseMenuUI;
        public Button selectButton;
        public GameObject optionOverlay;
        public Button optionSelectButton;
        public GameObject controlsOverlay;
        public Button controlSelectButton;
        public GameObject quitOverlay;
        public Button quitSelectButton;
        public bool statechangeTest;

        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
            controls.Player.Pause.performed += rptx => ResumePause();

            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;

            OnGameStateChanged(GameStateManager.Instance.CurrentGameState);
        }

        public void ResumePause()
        {
            //Pausiert das Spiel mit Events
            GameStates currentGameState = GameStateManager.Instance.CurrentGameState;
            GameStates newGameState = currentGameState == GameStates.Gameplay
                ? GameStates.Paused
                : GameStates.Gameplay;

            GameStateManager.Instance.SetState(newGameState);
        }

        private void OnGameStateChanged(GameStates newGameState)
        {
            pauseMenuUI.SetActive(newGameState == GameStates.Paused);
            statechangeTest = newGameState == GameStates.Paused;
            if(newGameState == GameStates.Paused)
            {
                EventSystem.current.SetSelectedGameObject(selectButton.gameObject);
                selectButton.OnSelect(new BaseEventData(EventSystem.current));
            }
        }

        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }

        #region ButtonMethoden
        public void EnableSettings()
        {
            optionOverlay.SetActive(true);
            EventSystem.current.SetSelectedGameObject(optionSelectButton.gameObject);
            optionSelectButton.OnSelect(new BaseEventData(EventSystem.current));
        }

        //der weiter Panel wird deaktiviert 
        public void DisableSettings()
        {
            optionOverlay.SetActive(false);
            EventSystem.current.SetSelectedGameObject(selectButton.gameObject);
            selectButton.OnSelect(new BaseEventData(EventSystem.current));
        }

        //der weiter Panel wird deaktiviert 
        public void EnableControls()
        {
            controlsOverlay.SetActive(true);
            EventSystem.current.SetSelectedGameObject(controlSelectButton.gameObject);
            controlSelectButton.OnSelect(new BaseEventData(EventSystem.current));
        }
        //der weiter Panel wird deaktiviert 
        public void DisableControls()
        {
            controlsOverlay.SetActive(false);
            EventSystem.current.SetSelectedGameObject(selectButton.gameObject);
            selectButton.OnSelect(new BaseEventData(EventSystem.current));
        }

        //ein weiter Panel wird aktiviert und �ber den pausecanvas gelegt
        public void EnableExit()
        {
            quitOverlay.SetActive(true);
            EventSystem.current.SetSelectedGameObject(quitSelectButton.gameObject);
            quitSelectButton.OnSelect(new BaseEventData(EventSystem.current));
        }

        //der weiter Panel wird deaktiviert
        public void DisableExit()
        {
            quitOverlay.SetActive(false);
            EventSystem.current.SetSelectedGameObject(selectButton.gameObject);
            selectButton.OnSelect(new BaseEventData(EventSystem.current));
        }

        //L�dt die Szene Menu und setzt die zeit wieder auf 1 ("normal")
        public void LoadHub()
        {
            ResumePause();
            SceneManager.LoadScene("Hub");
            Debug.Log("l�dt Hub");
        }

        //Das Spiel wird beendet
        public void QuitGame()
        {
            Debug.Log("Spiel beendet...");
            Application.Quit();
        }

        private void OnEnable()
        {
            controls.Enable();
        }
        private void OnDisable()
        {
            controls.Disable();
        }
        #endregion
    }
}
