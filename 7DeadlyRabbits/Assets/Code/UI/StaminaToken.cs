using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class StaminaToken : MonoBehaviour
    {
        public GameObject token1, token2, token3, token4, token5, tokenBG1, tokenBG2;
        public GameObject emptyStamina01, emptyStamina02;

        public int stamina;
        public float playerStamina;
        public AudioSource audioSource;
        public ParticleSystem staminaparticle;

        public  bool isStaminaUp2 = false;
        public  bool isStaminaUp1 = false;

        private void Start()
        {
            GameEvents.current.GetStamina += staminaUp;
            //Wenn beim Skilltree der Skill isStaminaUp2 oder isStaminaUp1 steigen die Stamina
            if (isStaminaUp2 = (PlayerPrefs.GetInt("isStaminaUp2") != 0))
            {
                stamina = 5;
                emptyStamina01.SetActive(true);
                emptyStamina02.SetActive(true);

            }

            if (isStaminaUp1 = (PlayerPrefs.GetInt("isStaminaUp1") != 0))
            {
                stamina = 4;
                emptyStamina01.SetActive(true);
            }

            if (!isStaminaUp2 && !isStaminaUp1)
            {
                stamina = 3;
            }
           
        }

        //StaminaBlock eingesammelt
        private void staminaUp()
        {
            audioSource.Play();
            playerStamina += 3;
            staminaparticle.Play();
        }

       //Playerstamina f�llt sich mit der zeit. 
       //playerstamina wird auf Stamina gerundet
        private void Update()
        {
            playerStamina += 0.2f * Time.deltaTime;

            if (isStaminaUp2 = (PlayerPrefs.GetInt("isStaminaUp2") != 0))
            {
                tokenBG2.SetActive(true);
                tokenBG1.SetActive(true);
                if (playerStamina >= 5)
                {
                    playerStamina = 5;
                }
            }

            if (isStaminaUp1 = (PlayerPrefs.GetInt("isStaminaUp1") != 0))
            {
                tokenBG1.SetActive(true);
                if (playerStamina >= 4)
                {
                    playerStamina = 4;
                }
            }

            if (!isStaminaUp2 && !isStaminaUp1)
            {
                if (playerStamina >= 3)
                {
                    playerStamina = 3;
                }
            }
            
            stamina = Mathf.FloorToInt(playerStamina);

            if (stamina < 0)
                stamina = 0;


            if (isStaminaUp2 = (PlayerPrefs.GetInt("isStaminaUp2") != 0))
            {
                if (stamina >= 5)
                {
                    stamina = 5;
                }
            }
            if (isStaminaUp1 = (PlayerPrefs.GetInt("isStaminaUp1") != 0))
            {
                if (stamina >= 4)
                {
                    stamina = 4;
                }
            }
            if (!isStaminaUp2 && !isStaminaUp1)
            {
                if (stamina >= 3)
                {
                    stamina = 3;
                }
            }

            //Switch/Case wenn Stamina verbraucht f�r sprite
            switch (stamina)
            {
                case 5:
                    token1.gameObject.SetActive(true);
                    token2.gameObject.SetActive(true);
                    token3.gameObject.SetActive(true);
                    token4.gameObject.SetActive(true);
                    token5.gameObject.SetActive(true);
                    break;
                case 4:
                    token1.gameObject.SetActive(true);
                    token2.gameObject.SetActive(true);
                    token3.gameObject.SetActive(true);
                    token4.gameObject.SetActive(true);
                    token5.gameObject.SetActive(false);
                    break;
                case 3:
                    token1.gameObject.SetActive(true);
                    token2.gameObject.SetActive(true);
                    token3.gameObject.SetActive(true);
                    token4.gameObject.SetActive(false);
                    token5.gameObject.SetActive(false);
                    break;
                case 2:
                    token1.gameObject.SetActive(true);
                    token2.gameObject.SetActive(true);
                    token3.gameObject.SetActive(false);
                    token4.gameObject.SetActive(false);
                    token5.gameObject.SetActive(false);
                    break;
                case 1:
                    token1.gameObject.SetActive(true);
                    token2.gameObject.SetActive(false);
                    token3.gameObject.SetActive(false);
                    token4.gameObject.SetActive(false);
                    token5.gameObject.SetActive(false);
                    break;
                case 0:
                    token1.gameObject.SetActive(false);
                    token2.gameObject.SetActive(false);
                    token3.gameObject.SetActive(false);
                    token4.gameObject.SetActive(false);
                    token5.gameObject.SetActive(false);

                    return;
            }
            
        }
    }
}
