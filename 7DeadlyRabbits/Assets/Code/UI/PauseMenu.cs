﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace sevensindemon
{
    public class PauseMenu : MonoBehaviour
    {
        public InputMaster controls;

        public bool gameIsPaused = false;

        public GameObject pauseMenuUI;
        public GameObject optionOverlay;
        public GameObject howToOverlay;
        public GameObject controlsOverlay;
        public GameObject quitOverlay;

        GameObject[] tagObjects;
        

        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
            controls.Player.Pause.performed += rptx => ResumePause();

            tagObjects = GameObject.FindGameObjectsWithTag("toDisable");
        }

        //Jedes Panel, welches über den Canvas gelegt wurde, wird deaktiviert,
        //der Pausecanvas wird deaktiviert, die zeit wird wieder auf 1 ("normal") gesezt
        //und die abfrage gameIsPaused ist falsch
        //Der PauseCanvas wird aktiviert, die zeit auf null gesetzt und die bool abfrage wird auf true gesetzt

        public void ResumePause()
        {
            if (!gameIsPaused)
            {
                Time.timeScale = 0f;
                pauseMenuUI.SetActive(true);
                gameIsPaused = true;
                
                //Ui Animation
                //anim.SetTrigger("OpenMenu");

                foreach(GameObject tag in tagObjects)
                {
                    tag.SetActive(false);
                }
            }
            else if (gameIsPaused)
            {
                Time.timeScale = 1f;
                pauseMenuUI.SetActive(false);
                gameIsPaused = false;
                optionOverlay.SetActive(false);
                howToOverlay.SetActive(false);
                quitOverlay.SetActive(false);

                foreach (GameObject tag in tagObjects)
                {
                    tag.SetActive(true);
                }
            }
        }

        public void Resume()
        {
                       
                Time.timeScale = 1f;
                pauseMenuUI.SetActive(false);
                gameIsPaused = false;
                optionOverlay.SetActive(false);
                //howToOverlay.SetActive(false);
                quitOverlay.SetActive(false);

            foreach (GameObject tag in tagObjects)
            {
                tag.SetActive(true);
            }
        }

        public void EnableSettings()
        {
            optionOverlay.SetActive(true);
        }

        //der weiter Panel wird deaktiviert 
        public void DisableSettings()
        {
            optionOverlay.SetActive(false);
        }

        public void EnableHowTo()
        {
            howToOverlay.SetActive(true);
        }

        //der weiter Panel wird deaktiviert 
        public void DisableHowTo()
        {
            howToOverlay.SetActive(false);
        }

        //der weiter Panel wird deaktiviert 
        public void EnableControls()
        {
            controlsOverlay.SetActive(true);
        }
        //der weiter Panel wird deaktiviert 
        public void DisableControls()
        {
            controlsOverlay.SetActive(false);
        }

        //ein weiter Panel wird aktiviert und über den pausecanvas gelegt
        public void EnableExit()
        {
            quitOverlay.SetActive(true);
        }

        //der weiter Panel wird deaktiviert
        public void DisableExit()
        {
            quitOverlay.SetActive(false);
        }

        //Lädt die Szene Menu und setzt die zeit wieder auf 1 ("normal")
        public void LoadHub()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("Hub");
        }

        //Das Spiel wird beendet
        public void QuitGame()
        {
            PlayerPrefs.DeleteAll();
            Application.Quit();
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}