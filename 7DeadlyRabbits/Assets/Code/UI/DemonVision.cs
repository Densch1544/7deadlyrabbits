using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class DemonVision : MonoBehaviour
    {
        public InputMaster controls;
        public GameObject canvas;
        public Animator anim;

        public float visionTimer;

        public GameObject[] visionObjects;

        public bool demonVisionOn;
       

        void Awake()
        {
            //Verbindung zum neuen Input System 
            controls = new InputMaster();
            controls.Player.DemonEyes.performed += dectx => VisionTrigger();
        }


        //jedes Gameobject mit dem Tag "DemonVisionObjects" wird angezeigt und wird nach dem Ablauf des Timers.
        void Start()
        {
            visionObjects = GameObject.FindGameObjectsWithTag("DemonVisionObjects");
            foreach (GameObject obj in visionObjects)
            {
                obj.SetActive(false);
            }
        }

        private void Update()
        {
            visionTimer -= 1 * Time.deltaTime;

            if(visionTimer < 0)
            {
                foreach (GameObject obj in visionObjects)
                {
                    obj.SetActive(false);
                    canvas.SetActive(false);
                }
                visionTimer = 0f;
            }
        }

        public void VisionTrigger()
        {
            //Der Vision Canvas wird aktiviert
            visionTimer = 7f;
            canvas.SetActive(true);
            anim.SetTrigger("Eyes");
            demonVisionOn = true;
        }

        public void VisionEnabled()
        {

            foreach (GameObject obj in visionObjects)
            {
                obj.SetActive(true);
            }
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
