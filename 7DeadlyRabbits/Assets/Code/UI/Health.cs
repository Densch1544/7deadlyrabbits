﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
 //holds 3 "heart" sprites , the sprite for the game over scenario and a soundeffect
    public class Health : MonoBehaviour
    {
        public GameObject heart1, heart2, heart3, heart4, heart5, gameOver;
        public GameObject player;
        public float health;
        public AudioSource audioSource;
        public AudioClip healSound;
        public AudioClip[] satanLaugh;
        public ParticleSystem healParticle;

        public GameObject tinte;

        private bool isGameOver = false;
        public  bool isHealthUp1 = false;
        public bool isHealthUp2 = false;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
            GameEvents.current.PlayerDamage += healthDown;
            GameEvents.current.GetHeal += healthUp;
            GameEvents.current.Deathzone += instaDeath;

            //Wenn beim Skilltree der Skill isHealthUp2 oder isHealthUp1 steigen die Leben
            if (isHealthUp2 = (PlayerPrefs.GetInt("isHealthUp2") != 0))
            {
                health = 5;                
            }
            if (isHealthUp1 = (PlayerPrefs.GetInt("isHealthUp1") != 0))
            {
                health = 4;
            }
            if(!isHealthUp2 && !isHealthUp1)
            {
                health = 3;
            }

            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(true);
            heart3.gameObject.SetActive(true);
            heart4.gameObject.SetActive(true);
            heart5.gameObject.SetActive(true);
            gameOver.gameObject.SetActive(false);
        }

        //Herzblock eingesammelt und man ein Herz zurückbekommt
        private void healthUp()
        {
            audioSource.clip = healSound;
            audioSource.Play();
            health += 1;
            healParticle.Play();

        }
        
        //KrankenBlock eingesammelt
        private void healthDown()
        {
            health -= 1;
        }

        //Deathzone
        private void instaDeath()
        {
            health = 0;
        }

        //switch cases for different scenarios- if 1 dmg is taken, disable 1 heart, and so on
        //0 hearts > Game over screen gets enabled + sound
        private void Update()
        {
            if (isHealthUp2 = (PlayerPrefs.GetInt("isHealthUp2") != 0))
            {
                if (health >= 5)
                {
                    health = 5;
                }
            }

            if (isHealthUp1 = (PlayerPrefs.GetInt("isHealthUp1") != 0))
            {
                if (health >= 4)
                {
                   health = 4;
                }
            }

            if (!isHealthUp2 && !isHealthUp1)
            {
                if (health >= 3)
                {
                    health = 3;
                }
            }

            switch (health)
            {
                case 5:
                   
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(true);
                    heart3.gameObject.SetActive(true);
                    heart4.gameObject.SetActive(true);
                    heart5.gameObject.SetActive(true);
                    break;
                case 4:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(true);
                    heart3.gameObject.SetActive(true);
                    heart4.gameObject.SetActive(true);
                    heart5.gameObject.SetActive(false);
                    break;
                case 3:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(true);
                    heart3.gameObject.SetActive(true);
                    heart4.gameObject.SetActive(false);
                    heart5.gameObject.SetActive(false);
                    break;
                case 2:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(true);
                    heart3.gameObject.SetActive(false);
                    heart4.gameObject.SetActive(false);
                    heart5.gameObject.SetActive(false);
                    break;
                case 1:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(false);
                    heart3.gameObject.SetActive(false);
                    heart4.gameObject.SetActive(false);
                    heart5.gameObject.SetActive(false);
                    break;
                case 0:
                    heart1.gameObject.SetActive(false);
                    heart2.gameObject.SetActive(false);
                    heart3.gameObject.SetActive(false);
                    heart4.gameObject.SetActive(false);
                    heart5.gameObject.SetActive(false);
                    tinte.SetActive(false);
                    gameOver.gameObject.SetActive(true);
                    player.SetActive(false);

                    if (!isGameOver)
                    {
                        audioSource.clip = satanLaugh[Random.Range(0, satanLaugh.Length)];
                        audioSource.Play();
                        isGameOver = true;
                    }
 //During Game over, we lock all input by setting the game time to 0.

                    break;

            }
        }
    }
}
