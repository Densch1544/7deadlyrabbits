using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
    public class AnyKey : MonoBehaviour
    {
        public float timer;
        [SerializeField]
        private float realTimer;
        public GameObject anytext;
        

        public GameObject toDisable01;
        public GameObject toDisable02;
        
        public bool ischeck = false;
        public string sceneName;

        private void Start()
        {
            realTimer = timer;
        }

        // Dr�cke anykey um zu Disablen. Funktioniert aber erst nach ablauf des Timers. 
        //"AnyKey" tauchst erst nach ablauf des times auf
        void Update()
        {
            realTimer -= 1 * Time.deltaTime;
            anytext.SetActive(false);

            if (realTimer < 0)
            {
                anytext.SetActive(true);
               
            }


            if (realTimer < 0 && Input.anyKeyDown)
            {
                toDisable01.SetActive(false);
                anytext.SetActive(false);
                if (!ischeck)
                {
                    toDisable02.SetActive(true);
                    anytext.SetActive(true);
                    realTimer = timer;
                    ischeck = true;

                }

                if (ischeck && realTimer < 0)
                {
                    SceneManager.LoadScene(sceneName);
                }
            }
        }
    }
}
