using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class LoopSound : MonoBehaviour
    {
        private AudioSource audioSource;

        void OnEnable()
        {
            audioSource = GetComponent<AudioSource>();
            //audio.clip = menu;
            audioSource.loop = true;
            audioSource.Play();
            //StartCoroutine(LoopAudio());
        }

        IEnumerator LoopAudio()
        {
            audioSource = GetComponent<AudioSource>();
            float length = audioSource.clip.length;

            while (true)
            {
                audioSource.Play();
                yield return new WaitForSeconds(length);
            }
        }
    }
}
