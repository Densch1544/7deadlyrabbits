using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


namespace sevensindemon
{
    public class CamerSwitcherOffice : MonoBehaviour
    {
        public CinemachineVirtualCamera zoomWorkCam;
        // Wenn in triggerläuft wird die Priortät der Camera runtergestellt, damit wird ein camerafahrt erstellt
        private void OnTriggerEnter2D(Collider2D collision)
        {

            zoomWorkCam.Priority = 9;
        }
    }
}
