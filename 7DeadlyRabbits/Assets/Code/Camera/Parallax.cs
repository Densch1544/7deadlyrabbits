﻿
using UnityEngine;

namespace PRTwo
{ 
    public class Parallax : MonoBehaviour
    {
        //camera position
        public Transform cam;
       // the "intensitiy" of the parralax effect. 1=same speed as camera
        public float parallaxEffect = 1f;
  
        private float startPos;

        void Start()
        { 
            //remember camera starting position
            startPos = transform.position.x;      
        }
    
        void FixedUpdate()
        {
            //calculate the new postion for the GO based on camera postion and parallax intensity
            float dist = (cam.position.x * parallaxEffect);
            //set new position
            transform.position = new Vector3(startPos + dist, transform.position.y, transform.position.z);         
            
        }
    }

}
