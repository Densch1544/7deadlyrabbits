using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace sevensindemon
{
    public class CameraSwitcherSatanBacktoNormal : MonoBehaviour
    {
        public CinemachineVirtualCamera vcamSatan;
        public GameObject triggervcamSatan;
        public GameObject triggervcamBackToNormal;


        // Wenn in triggerläuft wird die Priortät der Camera runtergestellt, damit wird ein camerafahrt erstellt
        private void OnTriggerEnter2D(Collider2D collision)
        {

            vcamSatan.Priority = 9;
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            triggervcamSatan.SetActive(true);
            triggervcamBackToNormal.SetActive(false);
        }
    }
}
