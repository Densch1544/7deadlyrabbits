using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace sevensindemon
{
    public class CameraSwitcher : MonoBehaviour
    {
        public CinemachineVirtualCamera vcam2;

        // Wenn Level gestartet wird die Priort�t der Camera runtergestellt, damit wird ein camerafahrt erstellt
        void Start()
        {
            vcam2.Priority = 9;
        }

        
    }
}
