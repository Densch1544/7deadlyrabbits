using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace sevensindemon
{
    public class CameraSwitcherAnyKey : MonoBehaviour
    {
        public CinemachineVirtualCamera zoomWorkCam;

        // Wenn dr�cken e  wird die Priort�t der Camera runtergestellt, damit wird ein camerafahrt erstellt
        private void Update()
        {
            if(Input.GetKey(KeyCode.E))
            {
                zoomWorkCam.Priority = 9;
            }
        }
    }
}
