﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class SimpleCameraShakeInCinemachine : MonoBehaviour {
    #region shortShakeValues
    public float shortShakeDuration;          // Time the Camera Shake effect will last
    public float shortShakeAmplitude;         // Cinemachine Noise Profile Parameter
    public float shortShakeFrequency;      // Cinemachine Noise Profile Parameter
    public float shortPivotOffsetX;
    public float shortPivotOffsetY;
    private float ShakeElapsedTime = 0f;
    #endregion

    #region midShakeValues
    public float midShakeDuration;          // Time the Camera Shake effect will last
    public float midShakeAmplitude;         // Cinemachine Noise Profile Parameter
    public float midShakeFrequency;      // Cinemachine Noise Profile Parameter
    public float midPivotOffsetX;
    public float midPivotOffsetY;
    private float midElapsedTime = 0f;
    #endregion

   
    // Cinemachine Shake
    public CinemachineVirtualCamera VirtualCamera;
    [SerializeField] private CinemachineBasicMultiChannelPerlin virtualCameraNoise;
    private void Awake()
    {
        VirtualCamera = this.GetComponent<CinemachineVirtualCamera>();
        virtualCameraNoise = VirtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
    }
  
    public void veryShortCamShake()
    {
        StartCoroutine(ShakeCamShort());
    }
    // Update is called once per frame
    void Update()
    {
        // TODO: Replace with your trigger
        //if (Input.GetKeyDown(KeyCode.Y))
       // {
            //ShakeElapsedTime = ShakeDuration;
           // StartCoroutine(ShakeCamShort());
           // Debug.Log("CamShakeON");
       // }

        // If the Cinemachine componet is not set, avoid update
        //if (VirtualCamera != null /*&& virtualCameraNoise != null*/)
        //{
        //    // If Camera Shake effect is still playing
        //    if (ShakeElapsedTime > 0)
        //    {
        //        // Set Cinemachine Camera Noise parameters
        //        virtualCameraNoise.m_AmplitudeGain = ShakeAmplitude;
        //        virtualCameraNoise.m_FrequencyGain = ShakeFrequency;

        //        // Update Shake Timer
        //        ShakeElapsedTime -= Time.deltaTime;
        //    }
        //    else
        //    {
        //        // If Camera Shake effect is over, reset variables
        //        virtualCameraNoise.m_AmplitudeGain = 0f;
        //        ShakeElapsedTime = 0f;
        //    }
        //}
    }
    IEnumerator ShakeCamShort()
    {
        virtualCameraNoise.m_AmplitudeGain = shortShakeAmplitude;
        virtualCameraNoise.m_FrequencyGain = shortShakeFrequency;
        virtualCameraNoise.m_PivotOffset.y = shortPivotOffsetY;
        virtualCameraNoise.m_PivotOffset.x = shortPivotOffsetX;
        virtualCameraNoise.m_AmplitudeGain = shortShakeAmplitude;
        yield return new WaitForSeconds(shortShakeDuration);

        virtualCameraNoise.m_AmplitudeGain = 0f;
                ShakeElapsedTime = 0f;
                
    }
}
