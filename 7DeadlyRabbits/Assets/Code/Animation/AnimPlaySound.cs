using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class AnimPlaySound : MonoBehaviour
    {
        public AudioSource sfxA;
        public AudioSource sfxB;
        public AudioSource sfxC;
        public AudioSource sfxD;

        //Ein Event welches man in animationen triggern kann. 
        //Es spielt sound
        public void PlaySoundA(AnimationEvent e)
        {
            sfxA.Play();
        }

        public void PlaySoundB(AnimationEvent e)
        {
            sfxB.Play();
        }
        public void PlaySoundC(AnimationEvent e)
        {
            sfxC.Play();
        }
        public void PlaySoundD(AnimationEvent e)
        {
            sfxD.Play();
        }

    }
}
