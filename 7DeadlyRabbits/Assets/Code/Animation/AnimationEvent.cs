using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{   
    public class AnimationEvent : MonoBehaviour
    {
        public string scene;
        //Ein Event welches man in animationen triggern kann. 
        //Es l�d die n�chste szene
        public void NextScene(AnimationEvent e)
        {
            SceneManager.LoadScene(scene);

        }

    }
}
