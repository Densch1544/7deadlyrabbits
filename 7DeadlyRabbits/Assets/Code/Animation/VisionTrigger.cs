using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class VisionTrigger : MonoBehaviour
    {
        public DemonVision demonvision;

        public void VisionAnimEvent(AnimationEvent e)
        {
            demonvision.VisionEnabled();
        }
        
    }
}
