using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class AnimTriggerCameraShake : MonoBehaviour
    {
        public SimpleCameraShakeInCinemachine cameraShake;

        //Ein Event welches man in animationen triggern kann. 
        //Es macht ein CameraShake
        public void CamShake(AnimationEvent e)
        {
            cameraShake.veryShortCamShake();
        }
    }
}
