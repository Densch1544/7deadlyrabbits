using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
    public class AnimLoader : MonoBehaviour
    {
        public string sceneName;
        //Ein Event welches man in animationen triggern kann. 
        //Es l�d die n�chste szene
        public void LoadThisScene(AnimationEvent e)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
