using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class AnimationTrigger : MonoBehaviour
    {
        public string triggerName;
        public Animator anim;
        public AudioSource sound;
        public bool makeSound;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            anim.SetTrigger(triggerName);
            if(makeSound)
            {
            sound.Play();
            }
        }
    }
}
