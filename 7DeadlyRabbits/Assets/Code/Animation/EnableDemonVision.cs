using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class EnableDemonVision : MonoBehaviour
    {
        public Animator anim;

        public void EnableVision(AnimationEvent e)
        { 
            anim.SetTrigger("Vision");
        }
    }
}
