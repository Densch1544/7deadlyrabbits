using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

namespace sevensindemon
{
    public class MiniNotebookAnim : MonoBehaviour
    {
        public Animator anim;
        public string[] sceneName;
        // Start is called before the first frame update
        void Start()
        {
            GameEvents.current.NewQuest += StartAnimation;
            GameEvents.current.OpenNoteBook += StopAnimation;
        }
        private void Update()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            for(int i =0; i < sceneName.Length; i++)
            {
                if (sceneName[i] == currentScene.name)
                {
                    this.gameObject.SetActive(false);
                }
            }
        }
        public void StartAnimation()
        {
            anim.SetTrigger("NewQuest");
        }
        public void StopAnimation()
        {
            anim.SetTrigger("Idle");
            anim.ResetTrigger("NewQuest");
            Debug.Log("StopBookAnim");
        }


    }
}
//SendMessage(StopAnimation,, Mini Notebook_Canvas);
//AnimatorTrigger(NewQuest,Mini Notebook_Canvas);