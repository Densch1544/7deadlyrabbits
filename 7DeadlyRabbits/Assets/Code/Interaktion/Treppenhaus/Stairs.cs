using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Stairs : MonoBehaviour
    {
        public InputMaster controls;
        public StairsCooldown stairsCooldown;
       
        public Vector3 nextFloor;
        public GameObject player;
        public GameObject humanPlayer;
        public bool canStairs = false;
        public GameObject fade;
    


        void Awake()
        {
            controls = new InputMaster();
            controls.Player.Interact.performed += stctx => GoUp();
            
        }

        
        //Wenn der Timer abgelaufen und man sich in dem Trigger der Treppe steht kann man damit interagieren
        public void GoUp()
        {
            if (canStairs && stairsCooldown.dontPassTimer == 0)
            {
                StartCoroutine(goingUpfast());
               
            }
        }  
        //ein Fade wird eingeschaltet, es wird kurz gewartet und in der zeit bis der Player teleportieren wird
        IEnumerator goingUpfast()
        {
            fade.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            player.transform.position = nextFloor;
            humanPlayer.transform.position = nextFloor;
            stairsCooldown.dontPassTimer = stairsCooldown.dontPassCountdown;

            fade.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                canStairs = true;

            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            canStairs = false;
            
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
