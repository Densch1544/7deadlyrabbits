using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class StairsCooldown : MonoBehaviour
    {
        public float dontPassCountdown = 1.5f; //Timerzeit
        public float dontPassTimer; // Timer

        
        void Start()
        {
            dontPassTimer = dontPassCountdown;
        }

        // Timer ist azufgelaufen
        void Update()
        {
            dontPassTimer -= Time.deltaTime;

            if (dontPassTimer < 0)
            {
                dontPassTimer = 0;
                
            }
        }
    }
}
