using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Mission : MonoBehaviour
    {
        public GameObject missionCanvas;
        public BoxCollider2D plant;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //L�uft der Player in den Trigger wird oben rechts die Mission angezeigt 

            if (collision.CompareTag("Player"))
            {
                missionCanvas.SetActive(true);
                plant.enabled = false;

            }
        }
    }
}
