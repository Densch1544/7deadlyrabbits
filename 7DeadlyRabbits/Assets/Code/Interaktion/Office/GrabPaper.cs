using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class GrabPaper : MonoBehaviour
    {
        public InputMaster controls;

        public bool isPickUp = false;
        public bool isDesk = false;

        public GameObject docement03;
        public GameObject docement02;
        public GameObject docement01;

        public GameObject sticky;


        void Awake()
        {
            //new Input System
            controls = new InputMaster();
            controls.Player.Interact.performed += dectx => PickUp();
        }
   

        void PickUp()
        {
            //Wenn man am Schreibtisch steht und man hat noch nichts aufgenommen,
            //nimmt man das paper auf
            
            if(isDesk && !isPickUp && docement03.activeInHierarchy)
            {
                sticky.SetActive(true);
                isPickUp = true;
                docement03.SetActive(false);
            }

            if (isDesk && !isPickUp && docement02.activeInHierarchy)
            {
                sticky.SetActive(true);
                isPickUp = true;
                docement02.SetActive(false);
            }

            if (isDesk && !isPickUp && docement01.activeInHierarchy)
            {
                sticky.SetActive(true);
                docement01.SetActive(false);
                isPickUp = true;
            }


        }

        // Der Player steht am Schreibtisch
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.CompareTag("Player"))
            {
                isDesk = true;

            }
        }

        // Der Player verl�sst am Schreibtisch
        private void OnTriggerExit2D(Collider2D collision)
        {
            isDesk = false;
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
