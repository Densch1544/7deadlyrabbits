using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class OfficeManager : MonoBehaviour
    {
        
     
        public GameObject paper01;
        public GameObject paper02;
        public GameObject paper03;

        public GameObject missonDoneToDo;
        public GameObject missonDoneNotes;

        public GameObject toEnable;
        public BoxCollider2D enterDoor;
        


         // Update is called once per frame
         void Update()
         {
            //Wenn alle drei Papers aktiviert sind werden wird Paul und die Mission deaktiviert und
            //der Boxcollider aktiviert
              if (paper01.activeInHierarchy && paper02.activeInHierarchy && paper03.activeInHierarchy)
              {
                toEnable.SetActive(true);
                  enterDoor.enabled = true;
                missonDoneToDo.SetActive(true);
                missonDoneNotes.SetActive(true);


            }
         }
        
    }
}
