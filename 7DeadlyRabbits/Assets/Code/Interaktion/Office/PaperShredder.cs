using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class PaperShredder : MonoBehaviour
    {
        public GrabPaper grabPaper;
        public InputMaster controls;
        public GameObject paper01;
        public GameObject paper02;
        public GameObject paper03;


        public Animator anim;

        public bool isShredder = false;

        // Start is called before the first frame update
        void Awake()
        {
            //new Input system
            controls = new InputMaster();
            controls.Player.Interact.performed += dectx => Shredder();
        }


        void Shredder()
        {
            if(isShredder == true)
            {
                //Ist der Player in der N�he des Shredders und hat noch kein Paper abgegeben aber schon aufgenommen
                //wird das erste paper aktiviert
                if(grabPaper.isPickUp == true && !paper01.activeInHierarchy)
                {
                    paper01.SetActive(true);
                    
                    grabPaper.isPickUp = false;

                    anim.SetTrigger("Shred");
                }
                //Ist der Player in der N�he des Shredders und hat schon ddas erste Paper abgegeben und das n�chste aufgenommen
                //wird das zweite paper aktiviert
                if (grabPaper.isPickUp == true && paper01.activeInHierarchy == true && !paper02.activeInHierarchy)
                {
                    paper02.SetActive(true);
                    
                    grabPaper.isPickUp = false;
                    anim.SetTrigger("Shred");
                }

                //hat er das letzte paper in der hand und hat das zweite schon abgegeben wird das letzte aktiviert
                if (grabPaper.isPickUp == true && paper02.activeInHierarchy == true)
                {
                    paper03.SetActive(true);
                    
                    grabPaper.isPickUp = false;
                    anim.SetTrigger("Shred");
                }
            }
        }

        //Der Player befindet sich in der n�he des Shredders
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                isShredder = true;

            }
        }

        //Der Player verl�sst die n�he des Shredders
        private void OnTriggerExit2D(Collider2D collision)
        {
            isShredder = false;
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
