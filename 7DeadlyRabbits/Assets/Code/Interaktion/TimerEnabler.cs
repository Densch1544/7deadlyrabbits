using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class TimerEnabler : MonoBehaviour
    {
        public float timer;

        public GameObject[] toEnable;


        // Nach einem Timer werden gameobjekte aktiviert
        void Update()
        {
            timer -= 1 * Time.deltaTime;
            if(timer < 0)
            {
                foreach (GameObject obj in toEnable)
                {
                    obj.SetActive(true);
                }
            }
        }


    }
}
