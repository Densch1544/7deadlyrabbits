using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using sevensindemon;


namespace sevensindemon
{
    public class GameEvents : MonoBehaviour
    {
        public static GameEvents current;

        private void Awake()
        {
            current = this;
        }

        //Das Sammelskript f�r alle GameEvents die in unserem Spiel benutzt werden

        #region Minigame
        public event Action<int> GetInk;
        public void CollectInk(int ink)
        {
            if(GetInk != null)
            {
                GetInk(ink);
            }
        }

        public event Action PlayerDamage;
        public void DamagePlayer()
        {

            if (PlayerDamage != null)
            {
                PlayerDamage();
            }
        }

        public event Action<int> BlockDamage;
        public void DamageBlock(int damage)
        {

            if (BlockDamage != null)
            {
                BlockDamage(damage);
            }
        }

        public event Action GetHeal;
        public void CollectHeal()
        {
            if (GetHeal != null)
            {
                GetHeal();
            }
        }

        public event Action Deathzone;
        public void InstaDeath()
        {
            if (Deathzone != null)
            {
                Deathzone();
            }
        }

        public event Action GetStamina;
        public void CollectStamina()
        {
            if (GetStamina != null)
            {
                GetStamina();
            }
        }

        public event Action GetCoffee;
        public void CollectCoffee()
        {
            if (GetCoffee != null)
            {
                GetCoffee();
            }
        }

        
        #endregion

        public event Action NewQuest;
        public void ReceiveNewQuest()
        {
            if (NewQuest != null)
            {
                NewQuest();
            }
        }
        public event Action OpenNoteBook;
        public void NoteOpen()
        {
            if (OpenNoteBook != null)
            {
                OpenNoteBook();
            }
        }


        #region Exp
        public event Action GetStrongXP;
        public void CollectStrongeCube()
        {
            if (GetStrongXP != null)
            {
                GetStrongXP();
            }
        }

        public event Action GetMidXP;
        public void CollectMidCube()
        {
            if (GetMidXP != null)
            {
                GetMidXP();
            }
        }

        public event Action GetWeakXP;
        public void CollectWeakCube()
        {
            if (GetWeakXP != null)
            {
                GetWeakXP();
            }
        }

        public event Action<int> GetXp;
        public void CollectXp(int xP)
        {

            if (GetXp != null)
            {
                GetXp(xP);
            }
        }
        #endregion
    }
}
