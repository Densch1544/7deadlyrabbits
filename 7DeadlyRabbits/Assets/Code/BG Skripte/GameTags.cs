using UnityEngine;

namespace PRTwo
{

/// <summary>
/// A static class where we directly access values without an instance of the class
/// This class is used for the Tags of Unity and the only spot where we need to change the values of the Tags after
/// we set them in the TagManager of Unity
/// </summary>
public static class GameTags
{
    public static string PLAYER               = "Player";
    public static string ENEMYMANAGER         = "EnemyManager";
    public static string DEATHZONE            = "DeathZone";
    public static string ENEMY                = "Enemy";
    public static string POWERUP              = "PowerUp";

        /// <summary>
        /// Static method to find a GameObject which the tag variable PLAYER
        /// </summary>
        /// <returns></returns>
        public static GameObject FindPlayer()
    {
        return GameObject.FindWithTag(PLAYER);
    }
}
}