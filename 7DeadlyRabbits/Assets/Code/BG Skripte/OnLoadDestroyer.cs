using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
    public class OnLoadDestroyer : MonoBehaviour
    {
        public string sceneName;
        public GameObject dialogueManager;
        // Start is called before the first frame update
        void Start()
        {
            dialogueManager = GameObject.FindGameObjectWithTag("Dialoge");
        }
        //Findet den dialogmanager und zerst�rt ihn

        private void Update()
        {
            
            if(dialogueManager != null)
            {
                Destroy(dialogueManager);

            }
        }
    }
}
