using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class DeathZone : MonoBehaviour
    {
        public void OnTriggerEnter2D(Collider2D other)
         {
            if(other.gameObject.tag == "Player")
            {
                //Wenn der Player in den Trigger kommt wird der Player ausgeschaltet
                //und der losescreen eingeschaltet
                GameEvents.current.InstaDeath();

            }

            //Bl�cke die im Trigger sind werden verst�rt
            if (other.gameObject.tag == "Block")
            {
                Destroy(other.gameObject);
            }
        }
    }
}
