using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;


namespace sevensindemon
{
    public class InkCollector : MonoBehaviour
    {
        

        public InputMaster controls;
        public int currentInk;
        public GameObject winPanel;
        public Slider slider;
        public TextMeshProUGUI percentageText;
        public GameObject player;

        public GameObject inkfullSound;

        public float moveTimer;
        public float countdownToSpeedUp = 30f; //Timerzeit
        private float timerToSpeedUp; // Timer
        public float speedUpValue;
        public float waitToSpeed;

        public Animator anim;
        public bool isWinning1;
        public bool isWinning2;
        public bool ispressed;

        

        void Awake()
        {
            //new InputSystem
            controls = new InputMaster();
            controls.Player.WinGameKey.performed += wctx => WinGame();
            controls.Player.WinGame1.performed += wctx => isWinning1 = true;
            controls.Player.WinGame2.performed += wctx => isWinning2 = true;

            controls.Player.WinGame1.canceled += wctx => isWinning1 = false;
            controls.Player.WinGame2.canceled += wctx => isWinning2 = false;

            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }
        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
        private void OnGameStateChanged(GameStates newGameState)
        {
            enabled = newGameState == GameStates.Gameplay;
        }
        private void Start()
        {
            GameEvents.current.GetInk += CollectInkUp;
            timerToSpeedUp = countdownToSpeedUp;
        }

        //Wenn Kranken oder inkbottle eingesammelt
        public void CollectInkUp(int inkAmount)
        {
            currentInk += inkAmount;            
        }

        //wenn 100 tinte dr�cke taste zum gewinnen
        public void WinGame()
        {
            if(!ispressed)
            {
                if (currentInk == 100)
                {
                    winPanel.SetActive(true);
                    player.SetActive(false);
                    ispressed = true;
                }

            }
        }

        void Update()
        {
            //Spielt animation und sound ab wenn ink voll
            if(currentInk >= 100)
            {
                anim.SetTrigger("InkFull");
                currentInk = 100;
                inkfullSound.SetActive(true);
            }
            else
            {
                inkfullSound.SetActive(false);
            }
            

            SetInk();
            TextUpdate();
            if(isWinning1 && isWinning2)
            {
                WinGame();
            }


            timerToSpeedUp -= Time.deltaTime; // Timer z�hlt in Sekunden runter

            if (timerToSpeedUp <= 0)
            {
                MoveFaster();
                timerToSpeedUp = waitToSpeed;
            }
        }

        //wenn der timer abgelaufen ist wird der gleiche timer verk�rzt
        public void MoveFaster()
        {
            moveTimer -= speedUpValue;
            if (moveTimer <= 1f)
            {
                moveTimer = 1f;
            }
        }

        //zeigt ink in einem fillbar an 
        public void SetInk()
        {
            slider.value = currentInk;
        }

        //zeigt ink in einer % an 
        public void TextUpdate()
        {
            percentageText.text = Mathf.RoundToInt(currentInk) + " %";
        }
        private void OnEnable()
        {
            controls.Enable();
        }
        private void OnDisable()
        {
            controls.Disable();
        }
    }
}

