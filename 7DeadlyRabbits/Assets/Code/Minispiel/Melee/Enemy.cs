using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sevensindemon
{
    public  class Enemy : MonoBehaviour
    {

        [SerializeField]
        private int currentHealth;

        private Rigidbody2D enemyRB;

        public bool isGoingLeft;
        public bool isGoingRight;
        public bool isGoingUp;
        public bool isGoingDown;

        private bool isZigZag;
        private bool isFly;

        public Vector2 forceDirectionFinal;
        private Vector2 forceDirectionLeft;
        private Vector2 forceDirectionRight;
        private Vector2 forceDirectionUp;
        private Vector2 forceDirectionDown;

        public float idleTimer = 3f;
        private float directionSwitchTimer;

        // Start is called before the first frame update
        void Awake()
        {
            enemyRB = this.GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            if (idleTimer > 0)
            {
                idleTimer -= 1 * Time.deltaTime;
                //Start Animation hier
            }
            else if (!isFly && !isZigZag)
            {
                Movement();

            }
            else if (!isZigZag)
            {
                ZigZagMovement();
            }
            else if (isFly)
            {
                FlyMovement();
            }
        }

        // Bewegt den Gegner in die jeweilige Richtung 
        public void Movement()
        {
            if (isGoingLeft)
            {
                forceDirectionLeft = new Vector2(-1f, 0f);
            }
            if (isGoingRight)
            {
                forceDirectionRight = new Vector2(1f, 0f);
            }
            if (isGoingUp)
            {
                forceDirectionUp = new Vector2(0f, 1f);
            }
            if (isGoingDown)
            {
                forceDirectionDown = new Vector2(0f, -1f);
            }
            forceDirectionFinal = forceDirectionLeft + forceDirectionRight + forceDirectionUp + forceDirectionDown;
            enemyRB.AddForce(forceDirectionFinal, ForceMode2D.Force);

        }

        public void ZigZagMovement()
        {

        }
        public void FlyMovement()
        {

        }
        
    }
}
