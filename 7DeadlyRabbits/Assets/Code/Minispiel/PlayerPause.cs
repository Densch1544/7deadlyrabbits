using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class PlayerPause : MonoBehaviour
    {
        public MonoBehaviour playerController;
        public MonoBehaviour playerDash;
        public MonoBehaviour playerShield;
        public MonoBehaviour playerInkplosion;
        private Animator anim;
        public BookManager bookmanager;

        void Awake()
        {
            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
            anim = GetComponent<Animator>();
            bookmanager = Object.FindObjectOfType<BookManager>();
        }
        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
        private void OnGameStateChanged(GameStates newGameState)
        {
            playerController.enabled = newGameState == GameStates.Gameplay;
            playerDash.enabled = newGameState == GameStates.Gameplay;
            playerShield.enabled = newGameState == GameStates.Gameplay;
            playerInkplosion.enabled = newGameState == GameStates.Gameplay;
            anim.enabled = newGameState == GameStates.Gameplay;
            if(bookmanager != null)
            {
            bookmanager.gameObject.SetActive(newGameState == GameStates.Gameplay);
            }
        }
    }
}
