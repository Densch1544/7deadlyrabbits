using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class TutorialDash : MonoBehaviour
    {
        public InputMaster controls;
        public Player player;

        public int dashDamage = 5;

        public static bool isDash;
        public Transform dashCollider;

        public float dashSpeed;
        private float dashTime;
        public float startDashTime;
        private int direction;

        public StaminaToken staminaToken;

        private Rigidbody2D body2D;
        private Animator anim;

        private AudioSource audioSource;
        public AudioClip[] dashClip;
        public float dashVolume;
        public AudioClip downdashClip;

        public GameObject dashToken;
        public GameObject dashToken2;

        private bool isPaused;

        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
            controls.Player.Dash.performed += cctx => NewDash();

            controls.Player.PenDashSkillLeft.performed += pltx => PenDashLeft();
            controls.Player.PenDashSkillRight.performed += prtx => PenDashRight();
            controls.Player.PenDashSkillDown.performed += pdtx => PenDashDown();

            body2D = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            audioSource = GetComponentInChildren<AudioSource>();

        }
        // Update is called once per frame
        void FixedUpdate()
        {
            DashAttack();
            AttackDirection();

            if (isDash)
            {
                dashTime -= Time.deltaTime;
            }

            if (dashTime <= 0)
            {
                dashTime = startDashTime;
                body2D.velocity = Vector2.zero;
                isDash = false;
               

                if (direction == 3)
                {
                    audioSource.clip = downdashClip;
                    audioSource.Play();
                }
            }
        }

        void AttackDirection()
        {
            if (Input.GetAxisRaw("Horizontal") == -1)
            {
                dashCollider.transform.localPosition = new Vector2(-0.4f, 0f);

                direction = 1;
            }
            else if (Input.GetAxisRaw("Horizontal") == 1)
            {
                dashCollider.transform.localPosition = new Vector2(0.4f, 0f);

                direction = 2;
            }
            else if (Input.GetAxisRaw("Vertical") == -1)
            {
                dashCollider.transform.localPosition = new Vector2(0f, -0.7f);
                dashCollider.transform.localRotation = Quaternion.Euler(0, 0, 90);

                direction = 3;
            }
        }

        public void NewDash()
        {
            if (!isDash && staminaToken.playerStamina >= 1)
            {
                if (dashToken.activeSelf)
                {
                    dashToken2.SetActive(true);
                }
                else
                {
                    dashToken.SetActive(true);
                }

                isDash = true;
                DashAttack();
                staminaToken.playerStamina -= 2;
                if (isDash)
                {
                    audioSource.clip = dashClip[Random.Range(0, dashClip.Length)];
                    audioSource.Play();

                    if (direction == 1)
                    {
                        anim.SetTrigger("HorizontalDash");
                        body2D.velocity = Vector2.left * dashSpeed;
                       

                    }
                    else if (direction == 2)
                    {
                        anim.SetTrigger("HorizontalDash");
                        body2D.velocity = Vector2.right * dashSpeed;
                        

                    }
                    else if (direction == 3)
                    {

                        anim.SetTrigger("VerticalDash");
                        body2D.velocity = Vector2.down * dashSpeed;
                        

                    }
                }
            }
        }


        public void PenDashLeft()
        {
            if (!isDash && staminaToken.playerStamina >= 2)
            {
                //audioSource.clip = dashClip[Random.Range(0, dashClip.Length)];
                audioSource.PlayOneShot(dashClip[Random.Range(0, dashClip.Length)], dashVolume);

                isDash = true;
                DashAttack();
                staminaToken.playerStamina -= 2;
                anim.SetTrigger("HorizontalDash");
                body2D.velocity = Vector2.left * dashSpeed;
               

                if(dashToken.activeSelf)
                {
                    dashToken2.SetActive(true);
                }
                else
                {
                    dashToken.SetActive(true);
                }
            }
        }
        public void PenDashRight()
        {
            if (!isDash && staminaToken.playerStamina >= 2)
            {
                //audioSource.clip = dashClip[Random.Range(0, dashClip.Length)];
                audioSource.PlayOneShot(dashClip[Random.Range(0, dashClip.Length)], dashVolume);

                isDash = true;
                DashAttack();
                staminaToken.playerStamina -= 2;
                anim.SetTrigger("HorizontalDash");
                body2D.velocity = Vector2.right * dashSpeed;
                

                if (dashToken.activeSelf)
                {
                    dashToken2.SetActive(true);
                }
                else
                {
                    dashToken.SetActive(true);
                }
            }
        }
        public void PenDashDown()
        {
            if (!isDash && staminaToken.playerStamina >= 2)
            {
                //audioSource.clip = dashClip[Random.Range(0, dashClip.Length)];
                audioSource.PlayOneShot(dashClip[Random.Range(0, dashClip.Length)], dashVolume);

                isDash = true;
                DashAttack();
                staminaToken.playerStamina -= 2;
                anim.SetTrigger("VerticalDash");
                body2D.velocity = Vector2.down * dashSpeed;
               

                if (dashToken.activeSelf)
                {
                    dashToken2.SetActive(true);
                }
                else
                {
                    dashToken.SetActive(true);
                }
            }
        }

        void DashAttack()
        {
            if (isDash)
            {
                RaycastHit2D hitInfo = Physics2D.Raycast(dashCollider.position, dashCollider.forward);
                Debug.DrawLine(dashCollider.position, dashCollider.forward, Color.green);

                if (hitInfo)
                {
                    CubeDisplay enemy = hitInfo.transform.GetComponent<CubeDisplay>();

                    if (enemy != null)
                    {
                        enemy.TakeDamage(dashDamage);
                    }
                }
            }
        }


        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
