using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class TokenOpen : MonoBehaviour
    {
        public GameObject token;
        public GameObject token1;

        
        public Animator animator;

        public void Update()
        {
            if(token.activeInHierarchy && token1.activeInHierarchy)
            {

                    animator.SetTrigger("Done");

            }
        }
    }
}
