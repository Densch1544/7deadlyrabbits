using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class TutorialImmortal : MonoBehaviour
    {
        public InputMaster controls;
        public Player player;

        [Header("Invincible")]
        public float invincibleCountdown = 8f;
        [SerializeField]
        private float invincibleTimer;
        public GameObject shieldSprite;

        public StaminaToken staminaToken;

        [Header("Sounds")]
        private AudioSource audioSource;
        public AudioClip[] damageclips;
        public AudioClip shieldClip;

        private Rigidbody2D body2D;
        private Animator anim;

        public GameObject immortalToken;

        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
            controls.Player.InvincibleSkill.performed += intx => Immortality();
            body2D = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            audioSource = GetComponentInChildren<AudioSource>();
            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }
        private void Start()
        {
            GameEvents.current.PlayerDamage += PlayerDamaged;
        }

        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameStates newGameState)
        {
            switch (newGameState)
            {
                case GameStates.Gameplay:
                    controls.Enable();
                    break;
                case GameStates.Paused:
                    controls.Disable();
                    break;
                default:
                    break;
            }
        }

        void FixedUpdate()
        {
            // Invinicble-Timer nachdem der Spielende getroffen wurde
            if (invincibleTimer < 1.5)
            {
                anim.SetBool("DamagedBool", false);
            }
            if (invincibleTimer > 0)
            {
                invincibleTimer -= 1 * Time.deltaTime;
            }
            else
            {
                //audioSource.Stop();
                shieldSprite.SetActive(false);
            }
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            //Spieler werden nicht konstant schaden zugef?gt
            if (invincibleTimer <= 0f)
            {
                //F?gt dem Spieler bei Ber?hrung dmg zu
                if (collider.gameObject.layer == 7)
                {
                    GameEvents.current.DamagePlayer();
                    PlayerDamaged();
                }
            }
        }
        private void PlayerDamaged() // Gamefeeel Teil der Damaged Players
        {
            audioSource.clip = damageclips[Random.Range(0, damageclips.Length)];
            audioSource.Play();
            anim.SetBool("DamagedBool", true);
            invincibleTimer = invincibleCountdown;
            //anim.ResetTrigger("Attack");
            //anim.ResetTrigger("Stomp");
            anim.SetTrigger("Damaged");
        }
        public void Immortality()
        {
                if (invincibleTimer <= 0 && staminaToken.playerStamina >= 2)
                {
                    invincibleTimer = invincibleCountdown;
                    staminaToken.playerStamina -= 2;
                    immortalToken.SetActive(true);
                    audioSource.clip = shieldClip;
                    audioSource.Play();
                    shieldSprite.SetActive(true);
                }
                else
                {
                    return;
                }            
        }

        //aktiviert/deaktiviert das Input System
        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
