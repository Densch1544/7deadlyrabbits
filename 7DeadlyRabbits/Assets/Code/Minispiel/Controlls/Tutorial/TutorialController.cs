using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class TutorialController : MonoBehaviour
    {
        #region Variablen
        public InputMaster controls;
        public Player player;

        public TutorialDetonation detonation;
        public TutorialImmortal immortal;
        public TutorialDash dash;

        //Movement des Spielers
        [Header("Movement/Jump")]
        [SerializeField]
        private float speed;
        [SerializeField]
        private float jumpHeight;
        float movementinput;
        //public bool canMove = true;
        public StaminaToken staminaToken;

        private Rigidbody2D body2D;
        public BoxCollider2D boxCollider2d;
        private bool m_FacingRight = true;
        private bool isGrounded;
        private bool canDoubleJump;
        public Transform groundCheck;
        private float originalGravity = 1;

        //Animator des Spielers
        private Animator anim;


        // Particle System
        public ParticleSystem dust;

        [Header("Coffee")]
        public AudioSource audioSourceBean;
        public bool isCoffee = false;
        public AudioSource audioSourceMusic;
        public ParticleSystem coffeeParticle;

        [Header("Invincible")]
        public float invincibleCountdown = 8f;
        private float invincibleTimer;
        public GameObject shieldSprite;

        //Attacke des Spielers
        [Header("Attacke")]
        public Transform attackPoint;
        [SerializeField]
        private LayerMask enemyLayers;
        [SerializeField]
        private LayerMask groundLayers;
        public float attackRange = 0.5f;
        public int attackDamage = 1;
        public float attackRate = 2f;
        private float nextAttackTime = 0f;
        private bool atkUp, atkDown, atkLeft, atkRight;

        public float coffeeTimer = 2.5f;
        public float coffeeCountDown;

        #endregion

        #region Start,Update etc
        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
            controls.Player.Movement.performed += actx => Attack();
            controls.Player.Jump.performed += jctx => Jump(jctx);

            controls.Player.Movement.performed += (mctx => Move(mctx));
            controls.Player.Movement.canceled += (mctx => Move(mctx));

            body2D = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            boxCollider2d = GetComponent<BoxCollider2D>();

            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }

        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameStates newGameState)
        {
            switch (newGameState)
            {
                case GameStates.Gameplay:
                    controls.Enable();
                    break;
                case GameStates.Paused:
                    controls.Disable();
                    break;
                default:
                    break;
            }
        }

        void Start()
        {
            originalGravity = body2D.gravityScale;
            staminaToken.playerStamina = 5;
            GameEvents.current.GetCoffee += CoffeeSpeedUp;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            anim.SetFloat("yVelocity", body2D.velocity.y);

            coffeeCountDown -= 1 * Time.deltaTime;
            if (coffeeCountDown <= 0)
            {
                coffeeCountDown = 0;
                audioSourceMusic.pitch = 1;
                coffeeParticle.Stop();
                attackRate = 2;
                isCoffee = false;
            }

            AttackDirection();

            if (detonation.canMove)
            {
                //Der Output vom NewInputsystem
                movementinput = controls.Player.Movement.ReadValue<float>();
                Vector3 currentPosition = transform.position;
                currentPosition.x += movementinput * speed * Time.deltaTime;
                transform.position = currentPosition;

            }

            //isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayers); //Groundcheck
            anim.SetBool("Jumping", !IsGrounded());
        }

        public void CoffeeSpeedUp()
        {
            isCoffee = true;
            coffeeCountDown += coffeeTimer;

            if (isCoffee)
            {
                attackRate = 4;
                coffeeParticle.Play();
                audioSourceBean.Play();
                audioSourceMusic.pitch = 1.35f;


            }
        }

        private bool IsGrounded()
        {
            float extraHeightText = 0.5f;
            RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.extents, 0f, Vector2.down, extraHeightText, groundLayers);

            Color rayColor;
            if (raycastHit.collider != null)
            {
                rayColor = Color.green;
            }
            else
            {
                rayColor = Color.red;
            }
            Debug.DrawRay(boxCollider2d.bounds.center + new Vector3(boxCollider2d.bounds.extents.x, 0), Vector2.down * (boxCollider2d.bounds.extents.y + extraHeightText), rayColor);
            Debug.DrawRay(boxCollider2d.bounds.center - new Vector3(boxCollider2d.bounds.extents.x, 0), Vector2.down * (boxCollider2d.bounds.extents.y + extraHeightText), rayColor);
            Debug.DrawRay(boxCollider2d.bounds.center - new Vector3(boxCollider2d.bounds.extents.x, boxCollider2d.bounds.extents.y + extraHeightText), Vector2.right * (boxCollider2d.bounds.extents.x * 2f), rayColor);

            
            return raycastHit.collider != null;
        }
        //aktiviert/deaktiviert das Input System
        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }

        #endregion

        #region Movement
        void Jump(InputAction.CallbackContext jctx)
        {
            if (IsGrounded())
            {
                isGrounded = false;
                body2D.gravityScale = 0;        //Gravitation wird kurz auf 0 gesetzt f?r besseres Spielverhalten
                body2D.velocity += Vector2.up * jumpHeight;
                body2D.gravityScale = originalGravity;
                canDoubleJump = true;

            }
            else if (canDoubleJump == true)
            {
                body2D.gravityScale = 0;
                body2D.velocity += Vector2.up * jumpHeight;
                body2D.gravityScale = originalGravity;
                canDoubleJump = false;
                //anim.SetTrigger("DoubleJump");
            }

            anim.SetBool("Jumping", true);
            dust.Play();
        }

        void Move(InputAction.CallbackContext mctx)
        {
            //Animationen und Richtungswechsel des Spielers
            float moveX = mctx.ReadValue<float>(); ;
            if (moveX > 0 && m_FacingRight)
            {

                //Flip();
                m_FacingRight = !m_FacingRight;
                this.GetComponentInChildren<SpriteRenderer>().flipX = true;

            }
            else if (moveX < 0 && !m_FacingRight)
            {

                //Flip();
                m_FacingRight = !m_FacingRight;
                this.GetComponentInChildren<SpriteRenderer>().flipX = false;

            }
            anim.SetBool("Running", moveX != 0);
        }
        #endregion

        #region Attack
        void Attack()
        {
            //Damit nur eine bestimmte Anzahl an Angriffen ind er Sekunde ausgef?hrt werden kann
            if (Time.time >= nextAttackTime)
            {
                //Attack();
                nextAttackTime = Time.time + 1f / attackRate;

                RaycastHit2D hitInfo = Physics2D.Raycast(attackPoint.position, attackPoint.forward * attackRange);
                Debug.DrawLine(attackPoint.position, attackPoint.forward * attackRange, Color.green);

                if (hitInfo)
                {
                    CubeDisplay enemy = hitInfo.transform.GetComponent<CubeDisplay>();

                    if (enemy != null)
                    {
                        enemy.TakeDamage(attackDamage);

                        if (atkDown)
                        {
                            anim.SetTrigger("Stomp");
                        }
                        else if (atkRight || atkLeft)
                        {
                            anim.SetTrigger("Attack");
                        }
                        else if (atkUp)
                        {
                            //anim.SetTrigger("");
                        }

                        atkLeft = false;
                        atkDown = false;
                        atkRight = false;
                        atkUp = false;
                        anim.SetTrigger("Idle");
                    }
                }
            }
        }
        void AttackDirection()
        {
            //Sorgt daf?r, dass der attackPoint sich mit dem Spieler mitrotiert, sodass in alle Richtugnen angegriffen werden kann
            if (Input.GetAxisRaw("Horizontal") == -1)
            {
                attackPoint.transform.localPosition = new Vector2(-0.5f, 0f);

                atkRight = true;

                Attack();
            }
            else if (Input.GetAxisRaw("Horizontal") == 1)
            {
                attackPoint.transform.localPosition = new Vector2(0.5f, 0f);

                atkLeft = true;

                Attack();
            }
            else if (Input.GetAxisRaw("Vertical") == 1)
            {
                attackPoint.transform.localPosition = new Vector2(0f, 0.5f);

                atkUp = true;

                Attack();
            }
            else if (Input.GetAxisRaw("Vertical") == -1)
            {
                attackPoint.transform.localPosition = new Vector2(0f, -0.8f);

                atkDown = true;

                Attack();

            }

        }
        #endregion
    }
}
