using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class DashIcon : MonoBehaviour
    {
        public StaminaToken stamina;
        public GameObject icon;

        //Zeigt an wann man den Dash machen kann.
        void Update()
        {
            if(stamina.playerStamina >= 2)
            {
                icon.SetActive(true);
            }
            else
            {
                icon.SetActive(false);
            }
        }
    }
}
