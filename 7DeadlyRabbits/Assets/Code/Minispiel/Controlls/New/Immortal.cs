using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Immortal : MonoBehaviour
    {
        public InputMaster controls;
        public Player player;

        [Header("Invincible")]
        public float invincibleCountdown = 8f;
        private float invincibleTimer;
        public GameObject shieldSprite;

        public StaminaToken staminaToken;
        public GameObject icon;

        [Header("Sounds")]
        private AudioSource audioSource;
        public AudioClip[] damageclips;
        public AudioClip shieldClip;

        private Rigidbody2D body2D;
        private Animator anim;


        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
            controls.Player.InvincibleSkill.performed += intx => Immortality();
            body2D = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            audioSource = GetComponentInChildren<AudioSource>();
        }
        private void Start()
        {
            GameEvents.current.PlayerDamage += PlayerDamaged;
        }
        void FixedUpdate()
        {
            ShowIcon();
            // Invinicble-Timer nachdem der Spielende getroffen wurde
            if (invincibleTimer < 1.5)
            {
                anim.SetBool("DamagedBool", false);
            }
            if (invincibleTimer > 0)
            {
                invincibleTimer -= 1 * Time.deltaTime;
            }
            else
            {
                //audioSource.Stop();
                shieldSprite.SetActive(false);
            }
        }

        //zeigt Icon wenn skill machbar
        public void ShowIcon()
        {
            if (player.CanUseInvincible())
            {
                icon.SetActive(true);
            }
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            //Spieler werden nicht konstant schaden zugef?gt
            if (invincibleTimer <= 0f)
            {
                //F?gt dem Spieler bei Ber?hrung dmg zu
                if (collider.gameObject.layer == 7)
                {
                    GameEvents.current.DamagePlayer();
                    PlayerDamaged();
                }
            }
        }
        private void PlayerDamaged() // Gasmefeeel Teil der Damaged Players
        {
            audioSource.clip = damageclips[Random.Range(0, damageclips.Length)];
            audioSource.Play();
            anim.SetBool("DamagedBool", true);
            invincibleTimer = invincibleCountdown;
            anim.SetTrigger("Damaged");
        }
        //Wenn benutzt keine schadensberechnung m�glich bis timer abgelaufen. Erst machbar wenn geskillt und genug Stamina
        public void Immortality()
        {
            if (player.CanUseInvincible() && staminaToken.playerStamina >= 2)
            {
                if (invincibleTimer <= 0)
                {
                    invincibleTimer = invincibleCountdown;
                    staminaToken.playerStamina -= 2;
                    audioSource.clip = shieldClip;
                    audioSource.Play();
                    shieldSprite.SetActive(true);

                }
                else
                {
                    return;
                }
            }
        }

        //aktiviert/deaktiviert das Input System
        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }

    }
}
