using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class DetonationIcon : MonoBehaviour
    {
        public StaminaToken stamina;
        public GameObject icon;


        //Zeigt an wann man den Inkplosion machen kann.
        void Update()
        {
            if (stamina.playerStamina >= 3)
            {
                icon.SetActive(true);
            }
            else
            {
                icon.SetActive(false);
            }
        }
    }
}
