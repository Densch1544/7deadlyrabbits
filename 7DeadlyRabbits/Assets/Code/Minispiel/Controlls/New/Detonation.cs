using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Detonation : MonoBehaviour
    {
        public InputMaster controls;
        public Player player;

        [Header("Explosion")]
        public Transform inkExplosionPoint;
        private float explosionRange = 2;
        private bool isInking;
        public float explosionValue;
        public int explosionDamage = 5;
        public GameObject faceInk;
        private bool canExplode;
        private bool badtiming;
        public SpriteRenderer rend;
        public SimpleCameraShakeInCinemachine simpleCameraShake;
        
        public bool canMove = true;
        

        public StaminaToken staminaToken;
        public GameObject icon;

        private Rigidbody2D body2D;
        private Animator anim;
        public Animator splashAnim;

        [Header("Sounds")]
        private AudioSource audioSource;
        public AudioClip explosionreadyClip;
        public AudioClip explosionClip;


        private void Awake()
        {
            //Neues Input System
            controls = new InputMaster();
           
            controls.Player.InkExplosionSkill.started += ietx => isInking = true;
            controls.Player.InkExplosionSkill.canceled += ietx => isInking = false;

            body2D = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            audioSource = GetComponentInChildren<AudioSource>();
        }

        void FixedUpdate()
        {
            ShowIcon();
            ChargeExplosion();

            if (!isInking)
            {
                canMove = true;
                explosionValue = 0;
                anim.SetTrigger("Idle");
                anim.SetFloat("ExplosionCharge", explosionValue);
            }
        }

        // zeichnet den Kreis
        void OnDrawGizmosSelected()
        {
            if (inkExplosionPoint == null)
            {
                return;
            }

            Gizmos.DrawWireSphere(inkExplosionPoint.position, explosionRange);
        }

        public void ShowIcon()
        {
            if(player.CanUseInkDetonation())
            {
                icon.SetActive(true);
            }
        }

        //L?dt f?r eine Ink-Explosion auf, desto l?nger aufgeladen wird, desto gr??er der Radius der Explosion
        public void ChargeExplosion()
        {
            if (staminaToken.playerStamina >= 3 && player.CanUseInkDetonation())
            {

                if (isInking)
                {
                    explosionValue += 1 * Time.deltaTime;
                    canMove = false;
                    anim.SetFloat("ExplosionCharge", explosionValue);
                }

                if (!isInking && canExplode)
                {
                    StartCoroutine(Explosion());
                }

                else if (explosionValue > 4 && isInking)
                {
                    badtiming = true;
                    explosionRange = 1;
                    isInking = false;
                    StartCoroutine(FaceInk());
                }

                else if (explosionValue > 3 && isInking)
                {
                    badtiming = false;
                    canExplode = true;
                }

                else if (explosionValue > 2 && isInking)
                {
                    audioSource.clip = explosionreadyClip;
                    audioSource.Play();
                }
            }
        }

        //Explosions Methode kostet Ink, kann aber daf?r eine Menge Bl?cke sofort zerst?ren
        IEnumerator Explosion()
        {
            splashAnim.Play("InkSplash");
            explosionValue = 0;
            canExplode = false;
            body2D.gravityScale = 0f;

            if (badtiming)
            {

                anim.SetTrigger("Damaged");
            }
            if (!badtiming)
            {
                anim.SetTrigger("ExplosionTrigger");
            }

            
            simpleCameraShake.veryShortCamShake();

            audioSource.clip = explosionClip;
            audioSource.Play();
            yield return new WaitForSeconds(0.1f);
            Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(inkExplosionPoint.position, explosionRange);

            foreach (Collider2D enemy in hitEnemies)
            {
                if (enemy.tag == "Block")
                {
                    enemy.GetComponent<CubeDisplay>().TakeDamage(explosionDamage);
                }
                if (enemy.tag == "Gloob")
                {
                    Destroy(enemy.gameObject);
                }
            }

            explosionRange = 2;
            body2D.gravityScale = 1f;
            staminaToken.playerStamina -= 3;
        }

        IEnumerator FaceInk()
        {
            faceInk.SetActive(true);
            yield return new WaitForSeconds(1);
            faceInk.SetActive(false);
        }

        //aktiviert/deaktiviert das Input System
        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }

    }
}
