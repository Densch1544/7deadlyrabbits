using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class ImmortalIcon : MonoBehaviour
    {
        public StaminaToken stamina;
        public GameObject icon;


        //Zeigt an wann man den Shield machen kann.
        void Update()
        {
            if (stamina.playerStamina >= 2)
            {
                icon.SetActive(true);
            }
            else
            {
                icon.SetActive(false);
            }
        }
    }
}
