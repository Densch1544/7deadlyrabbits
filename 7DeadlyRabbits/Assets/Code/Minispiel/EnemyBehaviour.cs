using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class EnemyBehaviour : MonoBehaviour
    {
        public float speed;

        private Rigidbody2D enemyRB;

        [SerializeField]
        private bool isGoingLeft;
        [SerializeField]
        private bool isGoingRight;
        [SerializeField]
        private bool isGoingUp;
        [SerializeField]
        private bool isGoingDown;

        private bool isPaused;

        public Vector2 forceDirectionFinal;
        private Vector2 forceDirectionLeft;
        private Vector2 forceDirectionRight;
        private Vector2 forceDirectionUp;
        private Vector2 forceDirectionDown;
        public float deathTimer;

        // Start is called before the first frame update
        void Awake()
        {
            enemyRB = this.GetComponent<Rigidbody2D>();
            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
           
        }

        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
        private void OnGameStateChanged(GameStates newGameState)
        {
            
            isPaused = newGameState == GameStates.Paused;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (isPaused)
            {
                enemyRB.velocity = Vector3.zero;
                deathTimer = 40f;
            }
            else
            {
                
                Movement();   
            }
            deathTimer -= Time.deltaTime;

            if (deathTimer <= 0)
            {
            Destroy(this.gameObject);
            }
        }

        // Bewegt den Gegner in die jeweilige Richtung 
        public void Movement()
        {
            if (isGoingLeft)
            {
                forceDirectionLeft = new Vector2(-speed, 0f);
            }
            if (isGoingRight)
            {
                forceDirectionRight = new Vector2(speed, 0f);
            }
            if (isGoingUp)
            {
                forceDirectionUp = new Vector2(0f, speed);
            }
            if (isGoingDown)
            {
                forceDirectionDown = new Vector2(0f, -speed);
            }
            forceDirectionFinal = forceDirectionLeft + forceDirectionRight + forceDirectionUp + forceDirectionDown;
            enemyRB.velocity = forceDirectionFinal;
            
        }
    }
}
