using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Trap : MonoBehaviour
    {
        public GameObject blocks;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            blocks.SetActive(true);
        }
    }
}
