using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Warnings : MonoBehaviour
    {
        public GameObject warning;


        //Wenn der big gegner den trigger betritt wird, zeigt sich eine bild am bildschirmrand
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.gameObject.tag == "Big")
            {
                warning.SetActive(true);

            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Big")
            {
                warning.SetActive(false);

            }
           
        }
    }
}
