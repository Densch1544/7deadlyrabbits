using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace sevensindemon
{
    public class CubeDisplay : MonoBehaviour
    {
        public Cube cubeInformation;

        private SpriteRenderer rend;
        private Sprite artWorkSprite;

        public int health;

        private bool strongCube;
        private bool midCube;
        private bool weakCube;

        private bool inkCube;
        public int ink;

        private bool healCube;
        private bool staminaCube;
        private bool coffee;
       

        private bool spawnsEnemies;
        private GameObject enemySpawned;
        private bool damagePlayer;

        public AudioSource blockSoundSource;
        public AudioClip[] blockSounds;
        private Vector3 startPosition;

        private ParticleSystem splinter;
        public Sprite state03;
        public Sprite state02;
        public Sprite state01;

        //setzt die Werte mit dem Scriptable Object gleich
        void Awake()
        {
            //holt die Variablen aus dem Scri Obj
            splinter = GetComponentInChildren<ParticleSystem>();
            rend = GetComponent<SpriteRenderer>();

            artWorkSprite = rend.sprite;

            staminaCube = cubeInformation.hasStamina;
            healCube = cubeInformation.canHeal;           
            strongCube = cubeInformation.isStrong;
            midCube = cubeInformation.isMid;
            weakCube = cubeInformation.isWeak;

            health = cubeInformation.healthPoints;

            inkCube = cubeInformation.hasInk;
            ink = cubeInformation.inkPoints;
            coffee = cubeInformation.hasSpeedUp;
            

            spawnsEnemies = cubeInformation.spawnsEnemy;
            enemySpawned = cubeInformation.spawnedEnemy;

            damagePlayer = cubeInformation.doesDamage;
        }

        //Erweckt Blaupause zum Leben

        public void TakeDamage(int damageAmount) //Block Damage
        {
            startPosition = this.transform.position;        
            health -= damageAmount;
            
            blockSoundSource.clip = blockSounds[Random.Range(0, blockSounds.Length)];
            blockSoundSource.Play();

            
            if (health <= 0)
            {
                health = 0;
                Die();
            }
            else if(!Dash.isDash)
            {            
            Tween Shaker = this.transform.DOShakePosition(.1f, 0.1f, 10, 90f, false, false);
            Shaker.SetAutoKill(false);
            }

            // Sprites �ndern sich je nach Block Health
            if (strongCube)
            {
                switch (health)
                {
                    case 3:
                        rend.sprite = state03;
                        splinter.Play();
                        break;
                    case 2:
                        rend.sprite = state02;
                        splinter.Play();
                        break;
                    case 1:
                        rend.sprite = state01;
                        splinter.Play();
                        break;
                    case 0:
                        splinter.Play();
                        break;
                }
            }
            if (midCube)
            {
                switch (health)
                {
                    case 2:
                        rend.sprite = state02;
                        splinter.Play();
                        break;
                    case 1:
                        rend.sprite = state01;
                        splinter.Play();
                        break;
                    case 0:
                        splinter.Play();
                        break;
                }
            }
            if (weakCube)
            {
                switch (health)
                {

                    case 1:
                        rend.sprite = state01;
                        splinter.Play();
                        break;
                    case 0:
                        splinter.Play();
                        break;

                }
            }
        }

        // Wenn die leben aufgebraucht sind wird der Block zerst?rt
        public void Die()
        {         
                if (inkCube)
                {
                    GameEvents.current.CollectInk(cubeInformation.inkPoints); // erh?ht die Tinte, wichtig f?r das Gewinnen
                }
                if (staminaCube)
                {
                    GameEvents.current.CollectStamina(); // erh?ht die Stamina, wichtig f?r die Skills
                }
                if (spawnsEnemies)
                {
                    Instantiate(enemySpawned, transform.position, transform.rotation); // Kann Gegner Spawnen
                }
                if (damagePlayer)
                {
                    GameEvents.current.DamagePlayer(); // F?gt dem Spieler bei der Zerst?rung DMG zu
                }
                if (healCube)
                {
                    GameEvents.current.CollectHeal(); // erh?ht die Leben des Players
                }
                if (strongCube)
                {
                    GameEvents.current.CollectStrongeCube(); // erh?ht Counter f?r die StrongCubes
                }
                if (midCube)
                {
                    GameEvents.current.CollectMidCube(); // erh?ht Counter f?r die MidCubes
                }
                if (weakCube)
                {
                    GameEvents.current.CollectWeakCube(); // erh?ht Counter f?r die WeakCubes
                }
                if (coffee)
                {
                    GameEvents.current.CollectCoffee(); //verdoppelt Attakrate
                }
                

            Destroy(gameObject);

        }

    }
}
