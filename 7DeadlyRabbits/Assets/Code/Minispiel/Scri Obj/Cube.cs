using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{

    [CreateAssetMenu(fileName = "New Cube", menuName = "Cube")]
    public class Cube : ScriptableObject
    {
        public Sprite artWork;
        public int healthPoints;

        public int xpPoints;

        public bool spawnsEnemy;
        public GameObject spawnedEnemy;
        public bool doesDamage;

        public bool hasInk;
        public int inkPoints;
        public bool canHeal;
        public bool hasStamina;
        public bool hasSpeedUp;
       

        public bool isStrong;
        public bool isMid;
        public bool isWeak;

       
        
        //Blaupause f?r verschiedene Cubes (Scriptable Objects)
    }
}
