using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class WeakCubeStates : MonoBehaviour
    {
        public CubeDisplay cubeDisplay;
        public int stateHealth;
        public SpriteRenderer rend;
        public ParticleSystem splinter;

        public Sprite state01;

        private void Start()
        {
            rend = GetComponent<SpriteRenderer>();
            splinter = GetComponentInChildren<ParticleSystem>();

        }
        // Update is called once per frame
        public void CheckStates()
        {
            stateHealth = cubeDisplay.health;
            switch (stateHealth)
            {
                
                case 1:
                    rend.sprite = state01;
                    splinter.Play();
                        break;
                case 0:
                    splinter.Play();
                        break;

            }
        }
    }
}
