using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class BigEnemySpawner : MonoBehaviour
    {
        public Spawn[] spawns;    //Liste zu spawnenden Objekten

        public InkCollector inkCollector;
        private float currentTimeToSpawn;
        public float spawnTimer;
        public GameObject container;

        public float rotationZ;
        public float rotationY;
        public float rotationX;

        void Awake()
        {
            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }

        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameStates newGameState)
        {
            enabled = newGameState == GameStates.Gameplay;
        }

        void Start()
        {
            currentTimeToSpawn = spawnTimer; //Zeit zur n?chsten Spawnwelle gleichgesetzt mit internen Timer

        }

      
        void Update()
        {
            if (currentTimeToSpawn > 0)
            {
                currentTimeToSpawn -= Time.deltaTime; // timer z?hlt runter
            }
            else
            {
                SpawnObject();
                currentTimeToSpawn = spawnTimer; // Objekt wird gespawnt und der Timer wieder Zeit zur n?chsten Spawnwelle gesetzt
            }

        }
    

        

        public void SpawnObject()
        {
            // W?hlt einen aus der Liste zu spawnenden Objekten aus und spawnt diesen
            int i = Random.Range(0, 100);

            for (int j = 0; j < spawns.Length; j++)
            {
                if(i >= spawns[j].minProbabillityRange && i <= spawns[j].maxProbabillityRange)
                {
                   var NewCube =  Instantiate(spawns[j].spawnObject, transform.position, transform.rotation * Quaternion.Euler (rotationX, rotationY, rotationZ));
                 
                    NewCube.transform.parent = container.transform;
                    break;
                }
            }
        }
    }
}
