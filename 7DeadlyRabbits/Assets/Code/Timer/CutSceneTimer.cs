using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class CutSceneTimer : MonoBehaviour
    {
        public float countdown = 3f; //Timerzeit
        private float timer;
        public GameObject slide1;
        public GameObject slide2;
        public GameObject canvas;
        

        // Start is called before the first frame update
        void Start()
        {
            timer = countdown;
        }

        // Nachdem Timer wird ein bild eingeschaltet und nach dem letzten der Canvas ausgeschaltet
        void Update()
        {
            timer -= Time.deltaTime;

            if(timer <= 0 && !slide1.activeInHierarchy)
            {
                slide1.SetActive(true);
                timer = countdown;
            }

            if (timer <= 0 && slide1.activeInHierarchy && !slide2.activeInHierarchy)
            {
                slide2.SetActive(true);
                timer = countdown;
            }

            if (timer <= 0 && slide2.activeInHierarchy)
            {
                canvas.SetActive(false);
                timer = countdown;
            }
        }
    }
}
