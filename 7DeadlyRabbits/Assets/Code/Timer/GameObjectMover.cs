using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class GameObjectMover : MonoBehaviour
    {
        public float moveTimer;
        private float currentTimer;
        public Transform cubeLocation;

        // Start is called before the first frame update
        void Start()
        {
            cubeLocation = GetComponent<Transform>(); // holt sich die Transform von eigener Position
            currentTimer = moveTimer; // setzt Inspector Timer gleich mit Internen Timer
        }

        // Update is called once per frame
        void Update()
        {
            //L�uft der Timer ab wird der W�rfel einen wert auf der Y achse verschoben
            currentTimer -= Time.deltaTime;
            if (currentTimer <= 0)
            {
                cubeLocation.position += new Vector3(0f, 1f, 0f);
                currentTimer = moveTimer;
            }
        }
    }
}
