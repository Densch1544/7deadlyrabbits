using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class Mover : MonoBehaviour
    {
        public Transform cubeLocation;

        public InkCollector inkCollector;

        void Awake()
        {
            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }
        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
        private void OnGameStateChanged(GameStates newGameState)
        {
            enabled = newGameState == GameStates.Gameplay;
        }

        // Start is called before the first frame update
        void Start()
        {
            cubeLocation = GetComponent<Transform>(); // holt sich die Transform von eigener Position
        }

        // Pro Sekunde des movetimers bewegen sich die Bl�cke nach oben 
        void Update()
        {
            cubeLocation.position += new Vector3(0f, 1/ inkCollector.moveTimer * Time.deltaTime, 0f);

        }
    }
}
