using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



namespace sevensindemon
{
    public class BlockCounter : MonoBehaviour
    {
        public TextMeshProUGUI strongText;
        public TextMeshProUGUI midText;
        public TextMeshProUGUI weakText;

        public TextMeshProUGUI strongTotalText;
        public TextMeshProUGUI midTotalText;
        public TextMeshProUGUI weakTotalText;

        public TextMeshProUGUI allTogethertext;

        public  int allTogether;

        private int strongTotal;
        private int midTotal;
        private int weakTotal;

        public int strongCounter;
        public int midCounter;
        public int weakCounter;

        public GameObject endCanvas;

        private LevelSystem levelSystem;
        private LevelSystemAnimated levelSystemAnimated;
        private PlayerSkills playerSkills;

        public bool isCalculate = false;


        private void Awake()
        {
            levelSystem = new LevelSystem();
            levelSystemAnimated = new LevelSystemAnimated(levelSystem);
            playerSkills = new PlayerSkills();
        }

        private void Start()
        {
            GameEvents.current.GetStrongXP += StrongCounterUP;
            GameEvents.current.GetMidXP += MidCounterUP;
            GameEvents.current.GetWeakXP += WeakCounterUP;
        }
        private void Update()
        {
            if(!isCalculate && endCanvas.activeSelf)
            {
                allTogether = (strongTotal + midTotal + weakTotal) * 5;
                isCalculate = true;
                PlayerPrefs.SetInt("allTogether", allTogether);
                PlayerPrefs.Save();
            }
            allTogethertext.text = allTogether.ToString();
        }

        //z�hlt die Blocklv03 die abgebaut wurden
        public void StrongCounterUP()
        {
            strongCounter += 1;
            strongText.text = strongCounter.ToString();
            strongTotal = (strongCounter * 3);
            strongTotalText.text = strongTotal.ToString();
            
        }

        //z�hlt die Blocklv02 die abgebaut wurden
        public void MidCounterUP()
        {
            midCounter += 1;
            midText.text = midCounter.ToString();
            midTotal = (midCounter * 2);
            midTotalText.text = midTotal.ToString();

        }

        //z�hlt die Blocklv01 die abgebaut wurden
        public void WeakCounterUP()
        {
            weakCounter += 1;
            weakText.text = weakCounter.ToString();
            weakTotal = (weakCounter * 1);
            weakTotalText.text = weakTotal.ToString();

        }
    }
}
