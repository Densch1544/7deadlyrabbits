using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class BlockSpawner : MonoBehaviour
    {
        public bool startSpawn;
        public InkCollector inkCollector;
        private float currentTimeToSpawn;
        public GameObject container;

        public Spawn[] spawns;    //Liste zu spawnenden Objekten

        void Awake()
        {
            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }
        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
        private void OnGameStateChanged(GameStates newGameState)
        {
            enabled = newGameState == GameStates.Gameplay;
        }

        void Start()
        {
            //currentTimeToSpawn = inkCollector.moveTimer; //Zeit zur n?chsten Spawnwelle gleichgesetzt mit internen Timer
            if (startSpawn)
            {
                SpawnObject();
            }
        }

      
        void Update()
        {
            if (currentTimeToSpawn > 0)
            {
                currentTimeToSpawn -= Time.deltaTime; // timer z?hlt runter
            }
            else if(!startSpawn)
            {
                SpawnObject();
                currentTimeToSpawn = inkCollector.moveTimer; // Objekt wird gespawnt und der Timer wieder Zeit zur n?chsten Spawnwelle gesetzt
            }

        }
    

       
        public void SpawnObject()
        {
            // W?hlt einen aus der Liste zu spawnenden Objekten aus und spawnt diesen
            int i = Random.Range(0, 100);

            for (int j = 0; j < spawns.Length; j++)
            {
                if(i >= spawns[j].minProbabillityRange && i <= spawns[j].maxProbabillityRange)
                {
                   var NewCube =  Instantiate(spawns[j].spawnObject, transform.position, transform.rotation);
                 
                    NewCube.transform.parent = container.transform;
                    break;
                }
            }
        }
    }
}
