using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    [System.Serializable]
    public class Spawn 
    {
        public GameObject spawnObject;
        public int minProbabillityRange = 0;
        public int maxProbabillityRange = 0;

    }
}
