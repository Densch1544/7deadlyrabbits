using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace sevensindemon
{
    public class Timer : MonoBehaviour
    {
        public float countdown = 30f; //Timerzeit
        public float timer; // Timer
        public Text countdownText; //Timer Text
        
       

        void Start()
        {
          timer = countdown; // Timerzeit wird zum Timer
        }

        void Update()
        {

            timer -= Time.deltaTime; // Timer zählt in Sekunden runter

            countdownText.text = Mathf.Round(timer).ToString(); // Nur ganze zahlen werden angezeit

            if (timer <= 0)
            {
               
            }
        }    
    }
}
