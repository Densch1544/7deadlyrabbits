using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

namespace sevensindemon
{
    public class LevelSystemAnimated
    {
        //Animiert die Levelanzeige f?rs langsame f?llen. updateTimerMax umso mehr umso langsamer f?llt sich die anzeige!
        //Fast die gleiche logik wie bei Levelsystem
        public event EventHandler OnExperienceChanged;
        public event EventHandler OnLevelChanged;

        private LevelSystem levelSystem;
        private bool isAnimating;
        private float updateTimer;
        private float updateTimerMax;


        private int level;
        private int experience;
        private int experienceToNextLevel;

        public LevelSystemAnimated(LevelSystem levelSystem)
        {
            SetLevelSystem(levelSystem);
            updateTimerMax = .008f;

            FunctionUpdater.Create(Update);
        }

        public void SetLevelSystem(LevelSystem levelSystem)
        {
            this.levelSystem = levelSystem;

            level = levelSystem.GetLevelNumber();
            experience = levelSystem.GetExperience();
            experienceToNextLevel = levelSystem.GetExperienceToNextLevel();

            levelSystem.OnExperienceChanged += levelSystem_OnExperienceChanged;
            levelSystem.OnLevelChanged += levelSystem_OnLevelChanged;
        }

        private void levelSystem_OnExperienceChanged(object sender, System.EventArgs e)
        {
            isAnimating = true;
        }

        private void levelSystem_OnLevelChanged(object sender, System.EventArgs e)
        {
            isAnimating = true;
        }

        private void Update()
        {
            if(isAnimating)
            {
                updateTimer += Time.deltaTime;
                while(updateTimer > updateTimerMax)
                {
                    updateTimer -= updateTimerMax;
                    UpdateAddExperience();
                }
            }
        }

        private void UpdateAddExperience()
        {
            if (level < levelSystem.GetLevelNumber())
            {
                AddExperience();
            }
            else
            {
                if (experience < levelSystem.GetExperience())
                {
                    AddExperience();
                }
                else
                {
                    isAnimating = false;
                }
            }
        }

        private void AddExperience()
        {
            experience++;
            if(experience >= experienceToNextLevel)
            {
                level++;
                experience = 0;
                if (OnLevelChanged != null) OnLevelChanged(this, EventArgs.Empty);
            }
            if (OnExperienceChanged != null) OnExperienceChanged(this, EventArgs.Empty);
        }

        public int GetLevelNumber()
        {
            return level;
        }

        public float GetExperienceNormalized()
        {
            return (float)experience / experienceToNextLevel;
        }


    }
}
