using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static sevensindemon.MinispielCompleted;

namespace sevensindemon
{
    public class PlayerExpManager : MonoBehaviour
    {
        public PlayerState playerState;

        void Start()
        {
            playerState.ExpGained(MinispielCompleted.exp);
            playerState.SkillPointsGained(MinispielCompleted.skillPoints);

            savePlayerState();
        }

        private void savePlayerState()
        {
            // JSON to Disk Magic!

            // Werte im static zurücksetzen
           
        }
        
    }
}
