using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class PlayerprefHub : MonoBehaviour
    {
        public int expTotal;
        public int currentExp;
        public int currentExpTotal;
        public int levelNumber;
        public LevelSystemAnimated levelSystem;
        public Player player;
        public PlayerSkills playerSkills;


        // Start is called before the first frame update
        void Start()
        {
            

            // PlayerPrefs are a key value pair storage
            // we utilize keys as input to look up certain values
            // we can store int, float, strings
            if (PlayerPrefs.HasKey("allTogether"))
            {
                //Speichert den wert aus den Level 
                //Berechnet die Exp in der leiste
                //Speichert den Expwert aus der leiste
                //Nimmt den wert mit ins Level
                //Speichert den wert aus den Level
                //Wert aus level und Exp aus leiste werden zusammengerechnet im Hub
                levelNumber = PlayerPrefs.GetInt("LevelNumber", levelNumber);
                expTotal =  PlayerPrefs.GetInt("expTotal", 0);
                currentExp = PlayerPrefs.GetInt("allTogether", 0);
                currentExpTotal = currentExp + expTotal - (levelNumber * 100);
                GameEvents.current.CollectXp(currentExpTotal);
                PlayerPrefs.SetInt("expTotal", currentExpTotal);
                levelNumber = levelSystem.GetLevelNumber();
                PlayerPrefs.SetInt("levelNumber", levelNumber);
                
            }

            
        }


        public void Update()
        {
            
        }


    }
}
