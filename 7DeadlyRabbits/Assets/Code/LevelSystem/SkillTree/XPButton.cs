using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class XPButton : MonoBehaviour
    {
        public LevelSystem levelSystem;

        private void Awake()
        {
            LevelSystem levelSystem = new LevelSystem();
            SetLevelSystem(levelSystem);
        }
        public void SetLevelSystem(LevelSystem levelSystem)
        {
            this.levelSystem = levelSystem;
        }
        public void FiveButton()
        {
            levelSystem.AddExperience(5);
            Debug.Log("du hast 5 erfahrung erhalten!!");
        }

        public void FiftyButton()
        {
            levelSystem.AddExperience(50);
            Debug.Log("du hast 50 erfahrung erhalten!!");
        }

        public void FiveHundredButton()
        {
            levelSystem.AddExperience(500);
            Debug.Log("du hast 500 erfahrung erhalten!!");
        }
    }
}
