﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace sevensindemon
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private ExperienceBar experienceBar;
        [SerializeField] private TextMeshProUGUI levelText;

        
        private LevelSystem levelSystem;
        private LevelSystemAnimated levelSystemAnimated;
        private PlayerSkills playerSkills;
        public Health health;
        public StaminaToken staminaToken;
        //public StaminaToken staminaToken;

       

        private void Awake()
        {
            
            levelSystem = new LevelSystem();
            levelSystemAnimated = new LevelSystemAnimated(levelSystem);
            playerSkills = new PlayerSkills();
            playerSkills.OnSkillUnlocked += PlayerSkills_OnSkillUnlocked;
        }
        
        private void PlayerSkills_OnSkillUnlocked(object sender, PlayerSkills.OnSkillUnlockedEventArgs e)
        {
            switch (e.skillType)
            {
                
                case PlayerSkills.SkillType.UnlockSkills:
                    Debug.Log("Unlock unlocked");
                    break;

                case PlayerSkills.SkillType.DoubleJump:
                    Debug.Log("DoubleJump unlocked");
                    break;

                case PlayerSkills.SkillType.Dash:
                    Debug.Log("Dash unlocked");
                    break;

                case PlayerSkills.SkillType.Invincible:
                    Debug.Log("Invincible unlocked");
                    break;

                case PlayerSkills.SkillType.InkDetonation:
                    Debug.Log("InkDetonation unlocked");
                    break;


                case PlayerSkills.SkillType.Health_1:
                    health.isHealthUp1 = true;
                    PlayerPrefs.SetInt("isHealthUp1", (health.isHealthUp1 ? 1 : 0));
                    Debug.Log("Health_1 unlocked");
                    break;

                case PlayerSkills.SkillType.Health_2:
                    health.isHealthUp1 = false;
                    health.isHealthUp2 = true;
                    PlayerPrefs.SetInt("isHealthUp1", (health.isHealthUp1 ? 1 : 0));
                    PlayerPrefs.SetInt("isHealthUp2", (health.isHealthUp2 ? 1 : 0));
                    Debug.Log("Health_2 unlocked");
                    break;

                case PlayerSkills.SkillType.Stamina_1:
                    Debug.Log("Stamina_1 unlocked");
                    staminaToken.isStaminaUp1 = true;
                    PlayerPrefs.SetInt("isStaminaUp1", (staminaToken.isStaminaUp1 ? 1 : 0));
                    break;

                case PlayerSkills.SkillType.Stamina_2:
                    staminaToken.isStaminaUp1 = false;
                    staminaToken.isStaminaUp2 = true;
                    PlayerPrefs.SetInt("isStaminaUp1", (staminaToken.isStaminaUp1 ? 1 : 0));
                    PlayerPrefs.SetInt("isStaminaUp2", (staminaToken.isStaminaUp2 ? 1 : 0));
                    Debug.Log("Stamina_2 unlocked");
                    break;
            }
        }
        
        private void Start()
        {

            levelSystemAnimated.OnExperienceChanged += LevelSystemAnimated_OnExperienceChanged;
            levelSystemAnimated.OnLevelChanged += LevelSystemAnimated_OnLevelChanged;
            levelText.SetText((levelSystemAnimated.GetLevelNumber() + 1).ToString());
            GameEvents.current.GetXp += CollectXpUp;
        }

        public void CollectXpUp(int xpAmount)
        {
            levelSystem.AddExperience(xpAmount);
            Debug.Log(xpAmount);
        }

        public PlayerSkills GetPlayerSkills()
        {
            return playerSkills;
        }

        public void LevelSystemAnimated_OnLevelChanged(object sender, System.EventArgs e)
        {
            // Level Up
            levelText.SetText((levelSystemAnimated.GetLevelNumber() + 1).ToString());
            playerSkills.AddSkillPoint();
            Debug.Log("player On level change");
        }

        private void LevelSystemAnimated_OnExperienceChanged(object sender, System.EventArgs e)
        {
            experienceBar.SetSize(levelSystemAnimated.GetExperienceNormalized());
            Debug.Log("player On xp change");
        }

        public bool CanUseSkillTree()
        {
            return playerSkills.IsSkillUnlocked(PlayerSkills.SkillType.UnlockSkills);
        }

        public bool CanUseInkDetonation()
        {
          return playerSkills.IsSkillUnlocked(PlayerSkills.SkillType.InkDetonation);
          
           
        }

        public bool CanUseInvincible()
        {
            return playerSkills.IsSkillUnlocked(PlayerSkills.SkillType.Invincible);
            
        }

        public bool CanUseDash()
        {
            return playerSkills.IsSkillUnlocked(PlayerSkills.SkillType.Dash);
            
        }

        public bool CanUseDoubleJump()
        {
            return playerSkills.IsSkillUnlocked(PlayerSkills.SkillType.DoubleJump);
        }
    }
}




