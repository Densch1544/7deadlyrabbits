﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class PlayerSkills
    {

        public event EventHandler OnSkillPointsChanged;
        public event EventHandler<OnSkillUnlockedEventArgs> OnSkillUnlocked;

        public class OnSkillUnlockedEventArgs : EventArgs 
        {
            public SkillType skillType;
        }

        public enum SkillType 
        {
            None,
            UnlockSkills,
            DoubleJump,
            Dash,
            Invincible,
            InkDetonation,
            Health_1,
            Health_2,
            Stamina_1,
            Stamina_2,
        }

        private List<SkillType> unlockedSkillTypeList;
        private  int skillPoints;

        public PlayerSkills() 
        {
            unlockedSkillTypeList = new List<SkillType>();
            LoadAllSkillProgress();
        }

        public void LoadAllSkillProgress()
        {
            foreach (SkillType e in Enum.GetValues(typeof(SkillType)))
            {
                if (HasUnlockedSkillType(e))
                {
                    unlockedSkillTypeList.Add(e);
                }
            }
        }

        public void SaveSkillISUnlocked(SkillType type)
        {
            PlayerPrefs.SetInt(type.ToString(), 1);
        }

        public bool HasUnlockedSkillType(SkillType type)
        {
            string key = type.ToString();
            return PlayerPrefs.HasKey(key) && PlayerPrefs.GetInt(key, 0) == 1;
        }

        public void AddSkillPoint() 
        {
            skillPoints++;
            OnSkillPointsChanged?.Invoke(this, EventArgs.Empty);
            Debug.Log("PlayerSkills skillpoints");
        }

        public  int GetSkillPoints() 
        {
            return skillPoints;
        }

        private void UnlockSkill(SkillType skillType) 
        {
            if (!IsSkillUnlocked(skillType)) 
            {
                SaveSkillISUnlocked(skillType);
                unlockedSkillTypeList.Add(skillType);
                OnSkillUnlocked?.Invoke(this, new OnSkillUnlockedEventArgs { skillType = skillType });
            }
        }

        public bool IsSkillUnlocked(SkillType skillType)
        {
            return unlockedSkillTypeList.Contains(skillType);
        }

        public bool CanUnlock(SkillType skillType) 
        {
            SkillType skillRequirement = GetSkillRequirement(skillType);

            if (skillRequirement != SkillType.None)
            {
                if (IsSkillUnlocked(skillRequirement)) 
                {
                    return true;
                } 
                else 
                {
                    return false;
                }
            } 
            else 
            {
                return true;
            }
        }

        public SkillType GetSkillRequirement(SkillType skillType) 
        {
            switch (skillType) 
            {
                case SkillType.DoubleJump:          return SkillType.UnlockSkills;

                case SkillType.InkDetonation:       return SkillType.DoubleJump;

                case SkillType.Invincible:          return SkillType.InkDetonation;

                case SkillType.Dash:                return SkillType.InkDetonation;

                case SkillType.Stamina_1:           return SkillType.Dash;

                case SkillType.Stamina_2:           return SkillType.Stamina_1;

                case SkillType.Health_1:            return SkillType.Invincible;

                case SkillType.Health_2:            return SkillType.Health_1;
            }

            return SkillType.None;
        }

        public bool TryUnlockSkill(SkillType skillType) 
        {
            if (CanUnlock(skillType)) 
            {
                if (skillPoints > 0) 
                {
                    skillPoints--;
                    OnSkillPointsChanged?.Invoke(this, EventArgs.Empty);
                    UnlockSkill(skillType);
                    return true;
                } 
                else 
                {
                    return false;
                }
            } 
            else 
            {
                return false;
            }
        }
    }
}
