﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using TMPro;

namespace sevensindemon
{
    public class UI_SkillTree : MonoBehaviour {

        [SerializeField] private Material skillLockedMaterial;
        [SerializeField] private Material skillUnlockableMaterial;
        [SerializeField] private SkillUnlockPath[] skillUnlockPathArray;
        [SerializeField] private Sprite lineSprite;
        [SerializeField] private Sprite lineGlowSprite;

        private PlayerSkills playerSkills;
        private List<SkillButton> skillButtonList;
        public  TextMeshProUGUI skillPointsText;

        public void SetPlayerSkills(PlayerSkills playerSkills) 
        {
            this.playerSkills = playerSkills;
            
            playerSkills.OnSkillUnlocked += PlayerSkills_OnSkillUnlocked;
            playerSkills.OnSkillPointsChanged += PlayerSkills_OnSkillPointsChanged;

            UpdateSkillPoints();

            skillButtonList = new List<SkillButton>();
            skillButtonList.Add(new SkillButton(transform.GetChild(0),          playerSkills, PlayerSkills.SkillType.UnlockSkills, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(1),          playerSkills, PlayerSkills.SkillType.DoubleJump, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(2),          playerSkills, PlayerSkills.SkillType.Dash, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(3),          playerSkills, PlayerSkills.SkillType.Invincible, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(4),          playerSkills, PlayerSkills.SkillType.InkDetonation, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(5),          playerSkills, PlayerSkills.SkillType.Health_1, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(6),          playerSkills, PlayerSkills.SkillType.Health_2, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(7),          playerSkills, PlayerSkills.SkillType.Stamina_1, skillLockedMaterial, skillUnlockableMaterial));
            skillButtonList.Add(new SkillButton(transform.GetChild(8),          playerSkills, PlayerSkills.SkillType.Stamina_2, skillLockedMaterial, skillUnlockableMaterial));
            
            
            UpdateVisuals();
        }

        private void PlayerSkills_OnSkillPointsChanged(object sender, System.EventArgs e) 
        {
            UpdateSkillPoints();
           
        }

        private void PlayerSkills_OnSkillUnlocked(object sender, PlayerSkills.OnSkillUnlockedEventArgs e) 
        {
            UpdateVisuals();
        }

        private  void UpdateSkillPoints() 
        {
          
            skillPointsText.SetText(playerSkills.GetSkillPoints().ToString());
        }

        
        private void UpdateVisuals()
        {
            foreach (SkillButton skillButton in skillButtonList) 
            {
                skillButton.UpdateVisual();
            }

            // Darken all links
            foreach (SkillUnlockPath skillUnlockPath in skillUnlockPathArray) 
            {
                foreach (Image linkImage in skillUnlockPath.linkImageArray) 
                {
                    linkImage.color = new Color(.5f, .5f, .5f);
                    linkImage.sprite = lineSprite;
                }
            }
        
            foreach (SkillUnlockPath skillUnlockPath in skillUnlockPathArray) 
            {
                if (playerSkills.IsSkillUnlocked(skillUnlockPath.skillType) || playerSkills.CanUnlock(skillUnlockPath.skillType)) {
                    // Skill unlocked or can be unlocked
                    foreach (Image linkImage in skillUnlockPath.linkImageArray) 
                    {
                        linkImage.color = Color.white;
                        linkImage.sprite = lineGlowSprite;
                    }
                }
            }
        }
        
        /*
         * Represents a single Skill Button
         * */
        private class SkillButton {

            private Transform transform;
            private Image image;
            private Image backgroundImage;
            private PlayerSkills playerSkills;
            private PlayerSkills.SkillType skillType;
            private Material skillLockedMaterial;
            private Material skillUnlockableMaterial;

            public SkillButton(Transform transform, PlayerSkills playerSkills, PlayerSkills.SkillType skillType, Material skillLockedMaterial, Material skillUnlockableMaterial) {
                this.transform = transform;
                this.playerSkills = playerSkills;
                this.skillType = skillType;
                this.skillLockedMaterial = skillLockedMaterial;
                this.skillUnlockableMaterial = skillUnlockableMaterial;

                image = transform.Find("Image").GetComponent<Image>();
                backgroundImage = transform.Find("Background").GetComponent<Image>();

               transform.GetComponent<Button_UI>().ClickFunc = () => 
               {
                    if (!playerSkills.IsSkillUnlocked(skillType)) 
                    {
                       
                        // Skill not yet unlocked
                        if (!playerSkills.TryUnlockSkill(skillType))
                        {
                          Tooltip_Warning.ShowTooltip_Static("Cannot unlock " + skillType + "!");
                        }

                    
                    }

                   if (playerSkills.IsSkillUnlocked(skillType))
                   {
                      
                       if (playerSkills.TryUnlockSkill(skillType))
                       {
                           Tooltip.ShowTooltip_Static("Unlock " + skillType + "!");
                       }
                   }
               };
            }

            
            public void UpdateVisual() 
            {
                if (playerSkills.IsSkillUnlocked(skillType)) 
                {
                    image.material = null;
                    backgroundImage.material = null;
                }
                else 
                {
                    if (playerSkills.CanUnlock(skillType)) 
                    {
                        image.material = skillUnlockableMaterial;
                        transform.GetComponent<Button_UI>().enabled = true;
                    }
                    else 
                    {
                        image.material = skillLockedMaterial;
                        backgroundImage.color = new Color(.3f, .3f, .3f);
                        transform.GetComponent<Button_UI>().enabled = false;
                    }
                }
            }
            

        }


        [System.Serializable]
        public class SkillUnlockPath 
        {
            public PlayerSkills.SkillType skillType;
            public Image[] linkImageArray;
        }

    }
}
