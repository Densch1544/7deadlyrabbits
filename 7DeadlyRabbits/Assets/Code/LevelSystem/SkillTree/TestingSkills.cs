﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class TestingSkills : MonoBehaviour {

        [SerializeField] private Player player;
        [SerializeField] private UI_SkillTree uiSkillTree;

        private void Start() 
        {
            uiSkillTree.SetPlayerSkills(player.GetPlayerSkills()); 
            //Hier ist der Fehler weil er die Buttons nicht findet und deswegen die funktion nicht abschließen kann. meiner Vermutung nach!
        }

    }
}
