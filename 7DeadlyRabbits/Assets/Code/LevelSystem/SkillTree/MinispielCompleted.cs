using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class MinispielCompleted 
    {
        public static int exp;
        public static int skillPoints;

        public static void MiniGameFinshedForSkill(int s)
        {
            skillPoints = s;

        }

        public static void MiniGameFinshedForExp( int e)
        {
            exp = e;
           
        }


        public static void Reset()
        {
            exp = 0;
            skillPoints = 0;
        }

        public class PlayerState
        {
            private int skillsTotal;
            private int expTotal;

            public void ExpGained(int exp)
            {
                expTotal += exp;
            }

            public void SkillPointsGained(int points)
            {
                skillsTotal += points;
            }
        }
    }
}
