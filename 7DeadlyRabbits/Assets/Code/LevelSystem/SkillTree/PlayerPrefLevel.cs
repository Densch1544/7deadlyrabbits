using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class PlayerPrefLevel : MonoBehaviour
    {
        public Player player;
        public PlayerSkills playerSkills;

        // Start is called before the first frame update
        void Start()
        {
            
            playerSkills.LoadAllSkillProgress();
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
