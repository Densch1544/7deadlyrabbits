﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window_Items : MonoBehaviour 
{
    public GameObject[] buttons;

	private void Start () 
    {
        foreach(GameObject button in buttons)
        {
            ItemInfo itemInfo = button.GetComponent<ItemInfo>();
            Tooltip_ItemStats.AddTooltip(itemInfo.transform, itemInfo.sprite, itemInfo.itemName, itemInfo.itemDescription);

        }
        
	}

}
