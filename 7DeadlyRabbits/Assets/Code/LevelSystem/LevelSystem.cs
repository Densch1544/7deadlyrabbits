using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class LevelSystem 
    {
        public event EventHandler OnExperienceChanged;
        public event EventHandler OnLevelChanged;

        private int level;
        private int experience;
        private int experienceToNextLevel;

        //Levelsystem wird auf anfang gesetzt
        public LevelSystem()
        {
            level = 0;
            experience = 0;
            experienceToNextLevel = 100;
        }

        //Wenn die experience mehr ist als die experienceToNextLevel dann werden sie subtrahiert
        //und bei Level wird eins dazugez�hlt
        public void AddExperience(int amount)
        {
            experience += amount;
            while(experience >= experienceToNextLevel)
            {
                level++;
                experience -= experienceToNextLevel;

                if (OnLevelChanged != null) OnLevelChanged(this, EventArgs.Empty);
            }
            if (OnExperienceChanged != null) OnExperienceChanged(this, EventArgs.Empty);
        }

        public int GetLevelNumber()
        {
            return level;
        }

        public float GetExperienceNormalized()
        {
            return (float)experience / experienceToNextLevel;
        }

        public int GetExperience()
        {
            return experience;
        }

        public int GetExperienceToNextLevel()
        {
            return experienceToNextLevel;
        }
    }
}
