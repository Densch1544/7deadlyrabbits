using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace sevensindemon
{
    public class LevelWindow : MonoBehaviour
    {
        //Durch dieses Skript wird das Levelsystem in der UI sichtbar
        public TextMeshProUGUI levelText;
        //public Image experienceBarImage;
        public LevelSystem levelSystem;
        public LevelSystemAnimated levelSystemAnimated;

        //referenzen werden gesetzt
        private void Awake()
        {
           // experienceBarImage = transform.Find("experienceBar").Find("bar").GetComponent<Image>();
            LevelSystem levelSystem = new LevelSystem();
            SetLevelSystem(levelSystem);
            LevelSystemAnimated levelSystemAnimated = new LevelSystemAnimated(levelSystem);
            SetLevelSystemAnimated(levelSystemAnimated);
        }

        //Erfahrungsslider zeigt erfahrung an 
        private void SetExperienceBarSize(float experienceNormalized)
        {
            //experienceBarImage.fillAmount = experienceNormalized;
        }

        //Levelanzeige 
        private void SetLevelNumber(int levelNumber)
        {
            levelText.text = " " + (levelNumber +1); 
        }

        //referenz
        public void SetLevelSystem(LevelSystem levelSystem)
        {
            this.levelSystem = levelSystem;
        }

        //Levelberechung mit langsam füllender Anzeige
        public void SetLevelSystemAnimated(LevelSystemAnimated levelSystemAnimated)
        {
            this.levelSystemAnimated = levelSystemAnimated;

            SetLevelNumber(levelSystemAnimated.GetLevelNumber());
            SetExperienceBarSize(levelSystemAnimated.GetExperienceNormalized());

            levelSystemAnimated.OnExperienceChanged += LevelSystemAnimated_OnExperienceChanged;
            levelSystemAnimated.OnLevelChanged += LevelSystemAnimated_OnLevelChanged;
        }

        private void LevelSystemAnimated_OnLevelChanged(object sender, EventArgs e)
        {
            SetLevelNumber(levelSystemAnimated.GetLevelNumber());
        }

        private void LevelSystemAnimated_OnExperienceChanged(object sender, EventArgs e)
        {
            SetExperienceBarSize(levelSystemAnimated.GetExperienceNormalized());
        }

        public void FiveButton()
        {
            levelSystem.AddExperience(5);
            Debug.Log("du hast 5 erfahrung erhalten!!");
        }

        public void FiftyButton()
        {
            levelSystem.AddExperience(50);
            Debug.Log("du hast 50 erfahrung erhalten!!");
        }

        public void FiveHundredButton()
        {
            levelSystem.AddExperience(500);
            Debug.Log("du hast 500 erfahrung erhalten!!");
        }
    }
}
