﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace sevensindemon { 
    //Script für die Bewegung des Spielers
    public class PlayerMovement_Quakus : MonoBehaviour
    {
    /*
        //private Player_Controls_Default controls;

        //Für die Bewegung
        [SerializeField]
        private Vector2 force;

        //Sprunghöhe
        private float jumpHeight = 15;      //again, hardcoded to save some time

        //Höhe des zweiten Sprunges
        [SerializeField]
        private Vector2 doubleJumpHeight;

        //Rigidbody vom Spieler
        private Rigidbody2D body2D;

        //Der Sprite Renderer des Spielers
        private SpriteRenderer spriteRenderer;

        //Die neue Sprite für den Spieler
        [SerializeField]
        private Sprite newSprite;

        //Erste Sprite des Spielers
        //public weil von einem anderen Script auf die Variabel zugegriffen wird
        public Sprite firstSprite;
        
        //Ob der Spieler springt und nochmal springen kann
        private bool isJumping = false;
        private bool canDoubleJump = false;
                        
        //Das PowerUps des Levels
        [SerializeField]
        private GameObject powerUp;

        //The Explosion
        [SerializeField]
        private ParticleSystem powerUpExplosion;
        
        //Referenz zum Game Manager Script
        private GameManager gameManager;
        
        //Der Tag des Game Managers
        [SerializeField]
        private string gameManagerTag = "GameManager";

        bool lastGrounded;      //Used to trigger onGroundedEnter

        private float originalGravity;

        private void Awake()
        {
            //controls = new Player_Controls_Default();   //Initializing a new instance
            controls.Enable();

            controls.Player.Jump.performed += ctx => JumpCheck();   //Subscribing the JumpCheck to the callback

            //Die Rigidbody Komponente am Spieler wird der Variabel zugeschreiben
            body2D = GetComponent<Rigidbody2D>();

            //Die SpriteRenderer Komponente am Spieler wird der Variabel zugeschrieben
            spriteRenderer = GetComponent<SpriteRenderer>();                        
        }

        private void Start()
        {
            //Der Variabel wird die Komponente zugeschrieben
            gameManager = GameObject.FindGameObjectWithTag(gameManagerTag).GetComponent<GameManager>();

            //Dem Spieler wird die Position vom letzten Checkpoint zugeschrieben
            transform.position = gameManager.lastCheckpointPosition;

            originalGravity = body2D.gravityScale;

            powerUpExplosion.Pause();
          
        }

        private void OnEnable()
        {
            //subscribing to the corresponding event-delegate
            Message<PickupItemEvent>.Add(OnPickupItemEvent);
        }

        private void OnDisable()
        {
            //unsubscribing from the corresponding delegate
            Message<PickupItemEvent>.Remove(OnPickupItemEvent);
        }

#region Old_Stuff
        //This is left in for historical/nostalgic reasons
        void OldMovement()
        {
            //Wenn man D drückt, läuft der Spieler nach rechts und das Sprite wird nicht gedreht
            if (Input.GetKey(KeyCode.D))
            {
                body2D.AddForce(force);
                transform.localEulerAngles = new Vector3(0, 0, 0);
            } //Wenn man A drückt, läuft der Spieler nach links und das Sprite wird gedreht
            else if (Input.GetKey(KeyCode.A))
            {
                body2D.AddForce(-force);
                transform.localEulerAngles = new Vector3(0, 180, 0);
            }
        }
#endregion

        private void Update()
        {
            //controls[2] is D and 1 is A
            RayChecks();
            if (controls.Player.Move.controls[1].IsPressed())
            {
                transform.localEulerAngles = new Vector3(0, 0, transform.localEulerAngles.z);
            } //Wenn man A drückt, (läuft der Spieler nach links) und das Sprite wird gedreht
            else if (controls.Player.Move.controls[2].IsPressed())
            {
                transform.localEulerAngles = new Vector3(0, 180, transform.localEulerAngles.z);
            }

            Vector3 toLerpto = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, Input.GetAxisRaw("Horizontal") * -10);
           // transform.rotation = Quaternion.Lerp(Quaternion.Euler(transform.localEulerAngles), Quaternion.Euler(toLerpto), 0.2f * Time.deltaTime * 100);
        }

        private void JumpCheck(){
            //Wenn der Spieler W drückt (or space) und in isJumping false ist...
            if (!isJumping)
            {
                //...springt der Spieler und isJumping wird auf true gestellt
                //body2D.AddForce(jumpHeight, ForceMode2D.Impulse);
                Jump(jumpHeight);
                isJumping = true;
                canDoubleJump = true;
            }
            else //Check ob der Spieler W drückt, isJumping true ist und es nicht die erste Form des Spielers ist
            {
                if (canDoubleJump && spriteRenderer.sprite != firstSprite)
                {
                    //Geht in die PowerUpOne Methode
                    PowerUpOne();                      
                }                
            }
            
        }

        private void FixedUpdate()
        {
            UpdatedMovement();
        }

        //Wenn der Spieler den boxCollider vom Objekt mit dem tag "ground" berührt
        //stellt sich isJumping auf false
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "ground")
            {
                isJumping = false;
            }                      
        }

        private void OnGroundedEnter(string collideString)
        {
            if(collideString == "ground")
            {
                isJumping = false;
            }
        }
        
        //Wenn der Spieler in den Trigger des PowerUps geht und Enter drückt...
        private void OnTriggerStay2D(Collider2D other)
        {
            if (controls.Player.Interact.controls[0].IsPressed() && other.gameObject.tag == "PowerUp")
            {
                //...wird das Objekt zerstört
                Destroy(other.gameObject);

                //The subscribed methods will be executed
                //Message.Raise(new PickupItemEvent(gameObject.name));                
            }            
        }
        
        private void OnPickupItemEvent(PickupItemEvent ev)
        {
            //Particle System gets played
            powerUpExplosion.Play();

            //Das Sprite wird geändert
            spriteRenderer.sprite = newSprite;     
        }
        
        //In der PowerUpOne Methode kann der Spieler einen zweiten Sprung ausführen
        //und canDoubleJump stellt sich auf false
        private void PowerUpOne()
        {
            //body2D.AddForce(doubleJumpHeight, ForceMode2D.Impulse);
            Jump(jumpHeight);
            canDoubleJump = false;
        }


        private float maxGrav = -3;
        private float maxSpeed = 6;     //hardcoded values, because otherwise they would need to be adjusted in every scene.
        private float speed = 0;
        float internGrav;
        bool jump;

        private void UpdatedMovement()
        {
            float direction = controls.Player.Move.ReadValue<float>();
            float x = Mathf.Round(direction);   //No AxisRaw so Mathf.Round should do the job
            float lastDir;
            if (x != 0) lastDir = x;
            speed = Mathf.Lerp(0, maxSpeed, Mathf.Abs(direction) * Time.deltaTime * 100);
            body2D.velocity = new Vector2(speed * x * Time.deltaTime * 50, body2D.velocity.y * Time.deltaTime * 50);
        }

        private void GravMod()
        {
            if (isGrounded)
            {
                internGrav = 0;
            }
            else
            {
                internGrav += Time.deltaTime * maxGrav;
            }
        }

        private void Jump(float strength)
        {
            body2D.gravityScale = 0;        //brief gravity disabling for a more conistent and controlled jump
            body2D.velocity += Vector2.up * jumpHeight;
            body2D.gravityScale = originalGravity;
        }


        //checks; Currently unused but may be necessary for further polishment, thus I left them in
        [SerializeField] private LayerMask lay;
        RaycastHit2D hit, lHit, rHit;
        bool isGrounded;
        int wallDir;

        void RayChecks()
        {
            hit = Physics2D.Raycast(transform.position, Vector2.up * -1, 0.99f, lay);
            lHit = Physics2D.Raycast(transform.position, Vector2.right * -1, 1.6f, lay);
            rHit = Physics2D.Raycast(transform.position, Vector2.right, 1.6f, lay);

            if (lHit.collider != null) wallDir = 1;
            else if (rHit.collider != null) wallDir = -1;
            else wallDir = 0;

            isGrounded = (hit.collider != null);

            if (!lastGrounded)
            {
                if (isGrounded)
                {
                    OnGroundedEnter(hit.collider.tag);
                    lastGrounded = true;
                }
            }
            else
            {
                if (!isGrounded)
                {
                    //Add OnGroundedExit here if necessary
                    lastGrounded = false;
                }
            }
           // Debug.Log(isGrounded);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, Vector2.up * -0.98f);
        }
*/
    }
}