using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class InputFromDevice : MonoBehaviour
    {
        /// <summary>
        /// Variable to set the speed of movement
        /// </summary>
        [SerializeField]
        private float speed;

        /// <summary>
        /// Direction vector to move to
        /// </summary>
        [SerializeField]
        private Vector3 direction;

        /// <summary>
        /// Bool to set if we use a gamepad or a keyboard
        /// </summary>
        [SerializeField]
        private bool useGamePad;

        private void Update()
        {
            // Depending on the bool value, we call different methods
            if (useGamePad)
            {
                MoveDirectlyByGamePadInput();
            }
            else
            {
                MoveDirectlyByKeyboardInput();
            }
        }

        /// <summary>
        /// New Input System: Using keyboard to move left or right by direct key presses
        /// </summary>
        private void MoveDirectlyByKeyboardInput()
        {
            // Getting the currently connected keyboard
            // DISCLAIMER: YOU DONT HAVE TO DO THIS IN UPDATE, CACHE THE VARIABLE IN AWAKE/START IN A REAL PRODUCTION
            Keyboard keyboard = Keyboard.current;

            // checking if the A Key is pressed
            if (keyboard.aKey.isPressed)
            {
                // Moving to the left
                transform.Translate(-direction.normalized * (speed * Time.deltaTime));
            }

            // checking if the D Key is pressed
            if (keyboard.dKey.isPressed)
            {
                // Moving to the right
                transform.Translate(direction.normalized * (speed * Time.deltaTime));
            }
        }


        /// <summary>
        /// New Input System: Using a gamepad to move left or right by direct key presses
        /// </summary>
        private void MoveDirectlyByGamePadInput()
        {
            // Getting the currently connected gamepad
            // DISCLAIMER: YOU DONT HAVE TO DO THIS IN UPDATE, CACHE THE VARIABLE IN AWAKE/START IN A REAL PRODUCTION
            Gamepad gamepad = Gamepad.current;

            // checking if the SquareButton on the connected Gamepad is pressed
            if (gamepad.squareButton.isPressed)
            {
                // Moving to the left
                transform.Translate(-direction.normalized * (speed * Time.deltaTime));
            }

            // checking if the CircleButton on the connected Gamepad is pressed
            if (gamepad.circleButton.isPressed)
            {
                // Moving to the left
                transform.Translate(direction.normalized * (speed * Time.deltaTime));
            }
        }
    }
}