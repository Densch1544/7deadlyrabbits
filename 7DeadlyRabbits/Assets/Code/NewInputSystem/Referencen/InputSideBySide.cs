using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class InputSideBySide : MonoBehaviour
    {
        
        private void Update()
        {
            // Comment one of the methods out to call it!
            
            //ExecuteOldInput();
            ExecuteNewInput();
        }

        /// <summary>
        /// A method to briefly remind how the old input system with Unity works to get direct key presses
        /// </summary>
        private void ExecuteOldInput()
        {
            if (Input.GetKey(KeyCode.A))
            {
                Debug.Log("Pressing the A Key on my keyboard,  yey!");
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("Pressing the A Key down once on my keyboard,  yey!");
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                Debug.Log("Lifting my finger from the A Key of my keyboard,  yey!");
            }
        }

        /// <summary>
        /// A method showing off how the new input system (package!) works in Unity to get direct key presses
        /// </summary>
        private void ExecuteNewInput()
        {
            // Getting the currently connected keyboard
            // DISCLAIMER: YOU DONT HAVE TO DO THIS IN UPDATE, CACHE THE VARIABLE IN AWAKE/START IN A REAL PRODUCTION
            Keyboard myKeyboard = Keyboard.current;

            // checking if the A Key is pressed all the time
            if (myKeyboard.aKey.isPressed)
            {
                Debug.Log("Pressing the A Key on my keyboard with the new Input System,  yey!");
            }

            // checking if the A Key is pressed at this very frame
            if (myKeyboard.aKey.wasPressedThisFrame)
            {
                Debug.Log("Pressing the A Key down once on my keyboard with the new Input System,  yey!");
            }

            // checking if the finger was lifted from the A Key this very frame
            if (myKeyboard.aKey.wasReleasedThisFrame)
            {
                Debug.Log("Lifting my finger from the A Key of my keyboard with the new Input System,  yey!");
            }
        }
    }
}