using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace sevensindemon
{
    public class InputFromAction : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        [SerializeField]
        private Vector2 movementDirection;

        private float dir;

        // In Order to listen to Unity Events from PlayerInput Component we need to specicy the context argument
        public void MoveLeft(InputAction.CallbackContext context)
        {
            // listening to the value of the input no matter the phase
            float input = context.ReadValue<float>();
            if (context.phase == InputActionPhase.Performed)
            {
                // When the context-phase hits the performed phase we do operations on it
                input *= -1;
            }

            // We set our dir to the read input at the end, so when the phase is for example canceled, we set it
            // so then basically "no input" is present
            dir = input;
        }
        
        // In Order to listen to Unity Events from PlayerInput Component we need to specicy the context argument
        public void MoveRight(InputAction.CallbackContext context)
        {
            // listening to the value of the input no matter the phase
            float input = context.ReadValue<float>();
            if (context.phase == InputActionPhase.Performed)
            {
                // When the context-phase hits the performed phase we do operations on it
                input *= 1;
            }

            // We set our dir to the read input at the end, so when the phase is for example canceled, we set it
            // so then basically "no input" is present

            dir = input;
        }

        private void Update()
        {
            // We move our transform based on the values of the variables
            transform.Translate(movementDirection.normalized * dir * (speed * Time.deltaTime));
        }
    }
}