using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using PixelCrushers;


namespace sevensindemon
{
    public class PlayerController : MonoBehaviour
    {
        public InputMaster controls;

        [SerializeField]
        private float speed, jumpSpeed;

        [SerializeField]
        private LayerMask ground;

        public Rigidbody2D body2D;
        public Collider2D col;
        public bool m_FacingLeft = true;

        public bool isJumping = false;
        private float originalGravity = 1;

        public Animator anim;


        private void Awake()
        {
            //Das eingestellte Movement vom New Inputssystem wird abgerufen
            controls = new InputMaster();

            controls.Player.Jump.performed += jctx => Jump();
            // New Input System Callback : Movement Happening
            controls.Player.Movement.performed += (mctx => Move(mctx));
            // New Input System Callback : Movement Stopped
            controls.Player.Movement.canceled += (mctx => Move(mctx));

            body2D = GetComponent<Rigidbody2D>();

            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        }

        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameStates newGameState)
        {
            enabled = newGameState == GameStates.Gameplay;
            anim.enabled = newGameState == GameStates.Gameplay;
        }
        void Start()
        {
            originalGravity = body2D.gravityScale;
        }

        void FixedUpdate()
        {
            //Der Output vom NewInputsystem
            float movementinput = controls.Player.Movement.ReadValue<float>();
            Vector3 currentPosition = transform.position;
            currentPosition.x += movementinput * speed * Time.deltaTime;
            transform.position = currentPosition;
           
        }

        void Jump()
        {
            if (!isJumping)
            {
                //Während des Sprunges wird die Grativation umgekehrt 
                isJumping = true;
                body2D.velocity += Vector2.up * jumpSpeed;
                body2D.gravityScale = originalGravity;

                anim.SetTrigger("Jump");
            }
        }

        void Move(InputAction.CallbackContext ctx)
        {
            // Get Movement Value from IS
            float movement = ctx.ReadValue<float>();

            //Debug.Log("Move gedrückt");

            if (movement > 0 && m_FacingLeft)
            {
                // ... flip the player.
                Flip();
            }
            else if (movement < 0 && !m_FacingLeft)
            {
                // ... flip the player.
                Flip();
            }

            // Notify Animator of movement yes/no for transitions
            anim.SetBool("Walk", movement != 0);
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingLeft = !m_FacingLeft;

            // Multiply the player's x local scale by -1.
                       
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;     
        }

        //Grounded true
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.tag == "Ground")
            {
                isJumping = false;
                anim.SetTrigger("Land");
            }

        }

        //Grounded flase
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Ground")
            {
                isJumping = true;
            }
        }

        //Aktiviert/Deaktiviert den Neuen Input
        private void OnEnable()
        {
            controls.Enable();
            //InputDeviceManager.RegisterInputAction("Interact", controls.Player.Interact);
        }
        private void OnDisable()
        {
            controls.Disable();
            //InputDeviceManager.UnregisterInputAction("Interact");
        }


        public void OnMoveCallback(InputAction.CallbackContext ctx)
        {
            Vector2 movement = ctx.ReadValue<Vector2>();
            anim.SetBool("Walk", movement.x != 0);
        }
    }
}
