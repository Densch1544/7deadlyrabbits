using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using PixelCrushers;

namespace sevensindemon
{
    public class NewInputRegistration : MonoBehaviour
    {
#if USE_NEW_INPUT

        private InputMaster controls;

        // Track which instance of this script registered the inputs, to prevent
        // another instance from accidentally unregistering them.
        protected static bool isRegistered = false;
        private bool didIRegister = false;

        void Awake()
        {
            controls = new InputMaster();
        }

        void OnEnable()
        {
            if (!isRegistered)
            {
                isRegistered = true;
                didIRegister = true;
                controls.Enable();
                InputDeviceManager.RegisterInputAction("Interact", controls.Player.Interact);
                InputDeviceManager.RegisterInputAction("Submit", controls.Player.Interact);
                InputDeviceManager.RegisterInputAction("Note", controls.Player.NoteSystem);
            }
        }

        void OnDisable()
        {
            if (didIRegister)
            {
                isRegistered = false;
                didIRegister = false;
                controls.Disable();
                InputDeviceManager.UnregisterInputAction("Note");
                InputDeviceManager.UnregisterInputAction("Submit");
                InputDeviceManager.UnregisterInputAction("Interact");
            }
        }

#endif
    }
}
