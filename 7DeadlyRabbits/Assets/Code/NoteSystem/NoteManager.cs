using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class NoteManager : MonoBehaviour
    {
        public InputMaster controls;

        public CanvasGroup bg;
        public CanvasGroup quest;
        public CanvasGroup options;
        public CanvasGroup notes;
        public CanvasGroup inventory;

        private bool isPause;

        [SerializeField]
        private int index = 1;

        void Awake()
        {
            //New Input System
            controls = new InputMaster();
            controls.Player.NoteSystem.performed += noctx => OpenBook();
            controls.Player.TurnOverRight.performed += noctx => TurnOverRight();
            controls.Player.TurnOverLeft.performed += noctx => TurnOverLeft();
            bg.alpha = 0f;
        }

        //Index minus eins und damit wird die seite umgeschlagen
        public void TurnOverRight()
        {
            index -= 1;  
        }

        //Index minus eins und damit wird die seite umgeschlagen
        public void TurnOverLeft()
        {
           index += 1;
        }

        //�ffnet das Buch/pausemenu
        public void OpenBook()
        {
            if(bg.alpha == 0f)
            {
                bg.alpha = 1f;
                isPause = true;
            }
            else
            {
                bg.alpha = 0f;
                isPause = false;
            }
        }

        //zeit anhalten wenn pause
        private void Update()
        {
            if (isPause)
            {
                Time.timeScale = 0f;
            }
            else
            {
                Time.timeScale = 1f;
            }

            if (index > 4)
            {
                index = 1;
            }
            
            switch (index)
            {
                case 4:
                    //Option
                    quest.alpha = 0f;
                    options.alpha = 1f;
                    inventory.alpha = 0f;
                    notes.alpha = 0f;
                    break;

                case 3:
                    //Notes
                    quest.alpha = 0f;
                    options.alpha = 0f;
                    inventory.alpha = 0f;
                    notes.alpha = 1f;
                    break;

                case 2:
                    //Inventar
                    quest.alpha = 0f;
                    options.alpha = 0f;
                    inventory.alpha = 1f;
                    notes.alpha = 0f;
                    break;

                case 1:
                    //Quest
                    quest.alpha = 1f;
                    options.alpha = 0f;
                    inventory.alpha = 0f;
                    notes.alpha = 0f;
                    break;

                case 0:
                    //Geschlossen
                    index = 4;
                    break;
            }
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
