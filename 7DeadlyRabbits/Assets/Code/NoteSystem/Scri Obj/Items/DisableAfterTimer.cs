using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class DisableAfterTimer : MonoBehaviour
    {
        public float timer = 2.5f;
        public float countDown;
        public GameObject sticky;

        // Start is called before the first frame update
        void Start()
        {
            countDown = timer;
        }

        // Update is called once per frame
        void Update()
        {
            countDown -= 1 * Time.deltaTime;

            if (countDown <= 0)
            {
                countDown = timer;
                sticky.SetActive(false);
            }
        }
    }
}
