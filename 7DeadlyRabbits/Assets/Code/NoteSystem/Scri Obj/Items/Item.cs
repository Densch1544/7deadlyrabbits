using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sevensindemon
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Item")]
    public  class Item : ScriptableObject
    {
        public string uiName;
        public int amount;
        public Sprite uiDisplay;
    }
}
