using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace sevensindemon
{
    public class ItemDisplay : MonoBehaviour
    {
        public Item itemInformation;

        public TextMeshProUGUI title;
        public TextMeshProUGUI number;
        public Image art;



        private void Awake()
        {
            title.text = itemInformation.uiName;
            number.text = itemInformation.amount.ToString();
            art.sprite = itemInformation.uiDisplay;
           
        }
    }
}
