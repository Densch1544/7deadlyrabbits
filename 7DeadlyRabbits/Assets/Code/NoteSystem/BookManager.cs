using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sevensindemon
{
    public class BookManager : MonoBehaviour
    {
        public InputMaster controls;

        public AudioSource audioSource;
        public AudioClip openBookClips;
        public AudioClip closeBookClips;
        public AudioClip[] turnBookClips;

        public GameObject notePanel;       
        public GameObject notes;
        public GameObject toDo;
        public GameObject options;

        public bool openBook;

        [SerializeField]
        private int index = 1;

        void Awake()
        {
            //New Input System
            controls = new InputMaster();
            controls.Player.NoteSystem.performed += noctx => OpenBook();
            controls.Player.TurnOverRight.performed += noctx => TurnOverRight();
            controls.Player.TurnOverLeft.performed += noctx => TurnOverLeft();

            GameStateManager.Instance.OnGameStateChanged += OnGameStateChanged;

            OnGameStateChanged(GameStateManager.Instance.CurrentGameState);
        }

        public void OpenBook()
        {
            //Pausiert das Spiel mit Events
            GameStates currentGameState = GameStateManager.Instance.CurrentGameState;
            GameStates newGameState = currentGameState == GameStates.Gameplay
                ? GameStates.Paused
                : GameStates.Gameplay;

            if(currentGameState == GameStates.Paused)
            {
                audioSource.clip = openBookClips;
                audioSource.Play();
            }
            if (currentGameState == GameStates.Gameplay)
            {
                audioSource.clip = closeBookClips;
                audioSource.Play();
            }

            GameStateManager.Instance.SetState(newGameState);
            GameEvents.current.NoteOpen();
        }
        private void OnGameStateChanged(GameStates newGameState)
        {
            notePanel.SetActive(newGameState == GameStates.Paused);
            openBook = newGameState == GameStates.Paused;

        }

            //Ist das Panel nicht aktiv wird es eingeschaltet und der Index wird auf eins gesetzt
            //Wenn Aktiv dann wird ausgeschaltet und Index auf null.

        //Index plus eins 
        public void TurnOverRight()
        {
            index += 1;
            audioSource.clip = turnBookClips[Random.Range(0, turnBookClips.Length)];
            audioSource.Play();
        }

        //Index minus eins
        public void TurnOverLeft()
        {
            if (index > 1)
            { 
                index -= 1;
                audioSource.clip = turnBookClips[Random.Range(0, turnBookClips.Length)];
                audioSource.Play();
            }
        }

        //Jede Indexzahl hat einen eigene Case d.h. Buchseite 
        private void Update()
        {
            if(index > 3)
            {
                index = 3;
            }

           
            switch (index)
            {
                case 3:
                    //Option
                    options.SetActive(true);
                    notes.SetActive(false);
                    toDo.SetActive(false);
                    break;

                case 2:
                    //Quest
                    toDo.SetActive(true);
                    options.SetActive(false);
                    notes.SetActive(false);
                    break;

                case 1:
                    //Noitzen
                    notes.SetActive(true);
                    options.SetActive(false);
                    toDo.SetActive(false);
                    break;

                case 0:
                    //Geschlossen
                    notePanel.SetActive(false);
                    options.SetActive(false);
                    toDo.SetActive(false);
                    break;
            }
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
        void OnDestroy()
        {
            GameStateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
    }
}
