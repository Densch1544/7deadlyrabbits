using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace sevensindemon
{
    public class LoadSceneButton : MonoBehaviour
    {
        public string sceneName;

        //L�d Szene wenn im trigger
        private void OnTriggerEnter2D(Collider2D collision)
        {
                SceneManager.LoadScene(sceneName);
            
        }

    }
}
