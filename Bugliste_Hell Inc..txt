Bugliste Hell Inc.
Version 2.0.1
Stand 25.02.2020


-----Controller Input-----
Finktioniert nicht 100%.
Gestestet nur mit einem Playstation Controller.

-----Sznenübergänge-----
Beim Tasten spammen kommen die Animationen nicht mehr hinterher und bleiben manchmal stecken


-----Dialogsystem-----
Beim Tasten spammen können Sequenzen geskippt werden


-----UI-----
Book Canvas Soundeffekte zum Seitenblättern sind abspielbar, auch wenn das Buch nicht geöffnet ist


-----Satans Office-----
Der Lava Shader funktionier noch nicht richtig


-----Dach-----
-Transformations Cutscene, einer von Damiens Sprites wird kurz unsichtbar
-Layer Fehler beim Rand ganz weit links


-----Sloth Appartment-----
2D Lighting Glitch bei der Küchenzeile im Hintergrund


-----Tutorial-----
-Mini Damien bleibt in seiner 'Damage' Animation stecken,nachdem der Dialog beendet ist.
-Skills triggern manchmal nicht die Animationen und Partikeleffekte der Blöcke
-Layerfehler im Hintergrund


-----Minispiel-----
-Mini Damiens Animationen haben keine sauberen Übergänge und bleiben manchmal in einem State stecken
-Der Abstand zwischen den Blöcken die hoch geschoben werden, wird mit der Zeit minimal größer
-Beim Einsammeln von "Stamina"-Token wird die Stamina manchmal nicht rauf gezählt
-Skills triggern manchmal nicht die Animationen und Partikeleffekte der Blöcke
"Snoths" Layer legen sich über die "Goops"


-----Minispiel Hub-----
-Da die Erfahrungspunkte gespeichert werden, gibt es unendlich viele Levelups und Skillpunkte. Man kann das Level also auch durch Pause>Exit beenden, und bekommt EXP.
-Die Skills im Skilltree kosten manchmal 2 Punkte, anstatt nur 1
-Gekaufte Skills können nochmal gekauft werden
-Skilltree ist nicht mit Controller anwählbar
